<?php

use Step\Acceptance\Login as Login,
    Step\Acceptance\ProfileUserStep as ProfileUser,
    Step\Acceptance\WorkingHostoryStep as WorkingHostory;

class Scenario5ECest
{
    public function _before(AcceptanceTester $I)
    {

    }

    public function _after(AcceptanceTester $I)
    {

    }

    // tests
    public function Scenario5(AcceptanceTester $I)
    {
        Login::login($I);

        ProfileUser::chooseProfile($I);
        ProfileUser::chooseButtonProfile($I);
        ProfileUser::getInfomationAccount($I);
        ProfileUser::clickOnAssignmentTab($I);
        ProfileUser::getTotalWorkingHistory($I);
        ProfileUser::clickOnButtonAddMore($I);

        WorkingHostory::fillFieldsWorkingHistory($I);
        WorkingHostory::clickOnButtonSaveWorkingHistory($I);
        WorkingHostory::checkMessageNotificationShownCorrect($I);

        ProfileUser::compareTotalWorkingHistory($I);
        ProfileUser::chooseProfile($I);
        ProfileUser::chooseLogout($I);
        ProfileUser::clickOnLoginAgain($I);

        Login::login($I);

        ProfileUser::chooseProfile($I);
        ProfileUser::chooseButtonProfile($I);
        ProfileUser::clickOnAssignmentTab($I);
        ProfileUser::compareTotalWorkingHistory($I);

        Login::logout($I);
    }
}
