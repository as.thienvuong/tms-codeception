<?php


use Step\Acceptance\CreateEmployeeStep as CreateEmployee,
    Step\Acceptance\SearchEmployeeStep as SearchEmployee,
    Step\Acceptance\SearchResultEmployeeStep as SearchResultEmployee,
    Step\Acceptance\Login as Login;

class Scenario1Cest
{
    public static function _before(AcceptanceTester $I)
    {

    }

    public function _after(AcceptanceTester $I)
    {

    }

    // Tests
    public function Scenario1(AcceptanceTester $I)
    {
        Login::login($I);

        SearchEmployee::clickCreateEmployee($I);

        CreateEmployee::fillField($I);
        CreateEmployee::chooseGender($I);
        CreateEmployee::selectedCombobox($I);
        CreateEmployee::chooseDateOfBirth($I);
        CreateEmployee::clickCreateEmployeeButton($I);
        CreateEmployee::checkMessageIsShownCorrect($I);

        #$this->_verifyMessageCreateNewEmployee($I);

        SearchEmployee::searchEmployee($I);
        SearchEmployee::clickSearchEmployeeButton($I);
        #$this->_searchEmployee($I);

        SearchResultEmployee::checkThatEmployeeWasCreated($I);
        #$this->_checkMessageIsShownCorrect($I);

        Login::logout($I);
    }

    public function _verifyMessageCreateNewEmployee(AcceptanceTester $I)
    {
        CreateEmployee::checkMessageIsShownCorrect($I);
    }

    public function _searchEmployee(AcceptanceTester $I)
    {
        SearchEmployee::searchEmployee($I);
        SearchEmployee::clickSearchEmployeeButton($I);
    }

    public  function _checkMessageIsShownCorrect(AcceptanceTester $I)
    {
        SearchResultEmployee::checkThatEmployeeWasCreated($I);
    }
}
