<?php

use Step\Acceptance\Login as Login,
    Step\Acceptance\SearchEmployeeStep as SearchEmployee,
    Step\Acceptance\SearchResultEmployeeStep as SearchResultEmployee,
    Step\Acceptance\ProfileUserStep as ProfileUser;

class Scenario4Cest
{
    public function _before(AcceptanceTester $I)
    {

    }

    public function _after(AcceptanceTester $I)
    {

    }

    // tests
    public function Scenario4(AcceptanceTester $I)
    {
        Login::login($I);

        SearchEmployee::clickSearchEmployeeButton($I);

        SearchResultEmployee::clickOnFirstResult($I);

        ProfileUser::clickOnEditButton($I);
        ProfileUser::clickOnEditCompannyInformation($I);
        ProfileUser::editCompanyEmail($I);
        ProfileUser::clickOnSaveButton($I);
        ProfileUser::checkMessageShownCorrect($I);
        ProfileUser::clickOnViewMode($I);
        ProfileUser::chooseProfile($I);
        ProfileUser::chooseLogout($I);
        ProfileUser::clickOnLoginAgain($I);

        Login::login($I);

        SearchEmployee::clickSearchEmployeeButton($I);

        SearchResultEmployee::checkEmailChangeCorrect($I);

        Login::logout($I);
    }
}
