<?php

use Step\Acceptance\Login as Login,
    Step\Acceptance\SearchProjectStep as SearchProject,
    Step\Acceptance\SearchResultProjectStep as SearchResultProject,
    Step\Acceptance\DetailProjectStep as DetailProject;

class Scenario3Cest
{
    public function _before(AcceptanceTester $I)
    {

    }

    public function _after(AcceptanceTester $I)
    {

    }

    // tests
    public function Scenario3(AcceptanceTester $I)
    {
        Login::login($I);

        SearchProject::searchProjectPage($I);
        SearchProject::clickButtonSearchProjectPage($I);

        SearchResultProject::choseFirstProject($I);

        DetailProject::editProjectName($I);
        DetailProject::checkMessageShownCorrect($I);

        SearchProject::searchProjectPage($I);
        SearchProject::clickButtonSearchProjectPage($I);

        SearchResultProject::checkThatProjecteWasCreated($I);

        Login::logout($I);
    }
}
