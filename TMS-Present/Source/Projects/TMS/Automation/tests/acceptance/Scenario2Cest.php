<?php

use Step\Acceptance\Login as Login,
    Step\Acceptance\CreateProjectStep as CreateProject,
    Step\Acceptance\SearchProjectStep as SearchProject,
    Step\Acceptance\SearchResultProjectStep as SearchResultProject;

class Scenario2Cest
{
    public function _before(AcceptanceTester $I)
    {
        //Login::login($I);
    }

    public function _after(AcceptanceTester $I)
    {
        //Login::logout($I);
    }

    // Tests
    public function Scenario2(AcceptanceTester $I)
    {
        Login::login($I);

        CreateProject::clickCreateProjectButton($I);
        CreateProject::fillField($I);
        CreateProject::selectedCombobox($I);
        CreateProject::startDate($I);
        CreateProject::clickButtonCreateNewProject($I);
        CreateProject::checkMessageShownCorrect($I);
        CreateProject::compareInputField($I);


        SearchProject::searchProjectPage($I);
        SearchProject::fillFieldSearchProject($I);
        SearchProject::clickButtonSearchProjectPage($I);

        SearchResultProject::checkThatProjecteWasCreated($I);

        Login::logout($I);
    }
}
