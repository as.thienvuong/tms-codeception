<?php
namespace Page;

class Login
{
    // include url of current page
    public static $URL = '';

    /**
     * Declare UI map for this page here. CSS or XPath allowed.
     * public static $usernameField = '#username';
     * public static $formSubmitButton = "#mainForm input[type=submit]";
     */

    /**
     * Basic route example for your current URL
     * You can append any additional parameter to URL
     * and use it in tests like: Page\Edit::route('/123-post');
     */
    public static function route($param)
    {
        return static::$URL.$param;
    }
    // Prepare data
    public static $login_username = 'adminHai';
    public static $login_password = 'sI3uPyn1';

    // Element
    public static  $txt_username = '//*[@id="username"]';
    public static $txt_password = '//*[@id="password"]';
    public static $btn_search =  '//*[@class="btn btn-primary btn-block"]';
    public static $btn_search_1 =  '(//div[@ui-view="employeesresult"]//div[@id="tbl-results"]//a)[]';
}
