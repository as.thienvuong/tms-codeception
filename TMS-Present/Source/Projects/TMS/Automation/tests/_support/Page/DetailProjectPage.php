<?php
namespace Page;

class DetailProjectPage
{
    // include url of current page
    public static $URL = '';

    /**
     * Declare UI map for this page here. CSS or XPath allowed.
     * public static $usernameField = '#username';
     * public static $formSubmitButton = "#mainForm input[type=submit]";
     */

    /**
     * Basic route example for your current URL
     * You can append any additional parameter to URL
     * and use it in tests like: Page\Edit::route('/123-post');
     */
    public static function route($param)
    {
        return static::$URL.$param;
    }

    public static $btn_edit_project = '//*[@class="col-md-4 ng-scope"]//*[@id="btnEdit"]';
    public static $txt_project_name = '//div[@class="col-md-4 col-md-offset-2"]//input[@id="name"]';
    public static $btn_save_edit_project = '//*[@class="col-md-4"]//*[@id="btnEdit"]';
    public static $message_notification = '//span[@data-notify = "message"]';
}
