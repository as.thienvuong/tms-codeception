<?php
namespace Page;

class SearchResultProjectPage
{
    // include url of current page
    public static $URL = '';

    /**
     * Declare UI map for this page here. CSS or XPath allowed.
     * public static $usernameField = '#username';
     * public static $formSubmitButton = "#mainForm input[type=submit]";
     */

    /**
     * Basic route example for your current URL
     * You can append any additional parameter to URL
     * and use it in tests like: Page\Edit::route('/123-post');
     */
    public static function route($param)
    {
        return static::$URL.$param;
    }

    public static $first_result_project ='(//div[@ui-view="projectsresult"]//div[@id="tbl-results"]//a)[1]';

    public static $table_result_project = '//div[@ui-view="projectsresult"]//tbody//tr';


}
