<?php
namespace Page;

use _generated\AcceptanceTesterActions;

class CreateEmployeePage
{
    // include url of current page
    public static $URL = '';

    /**
     * Declare UI map for this page here. CSS or XPath allowed.
     * public static $usernameField = '#username';
     * public static $formSubmitButton = "#mainForm input[type=submit]";
     */

    /**
     * Basic route example for your current URL
     * You can append any additional parameter to URL
     * and use it in tests like: Page\Edit::route('/123-post');
     */
    public static function route($param)
    {
        return static::$URL.$param;
    }
    // Element
    // Text box
    public static $txt_username = '//*[@id="username"]';
    public static $txt_firstname = '//*[@id="firstname"]';
    public static $txt_lastname = '//*[@id="lastname"]';
    public static $txt_address = '//*[@id="address"]';
    public static $txt_email = '//*[@id="cemail"]';
    // Radio button
    public static $rdb_gender = '//input[@value="0"]';
    // Selected
    public static $cbb_status = '//*[@id="stt"]';
    public static $value_cbb_status = '//*[@id="stt"]//*[@value="1 "]';
    public static $cbb_department = '//*[@id="department"]';
    public static $value_cbb_department = '//*[@id="department"]//*[@label="SD - Software Development"]';
    public static $cbb_competence = '//*[@id="competence"]';
    public static $value_cbb_competence = '//*[@id="competence"]//*[@label="QC"]';
    public static $cbb_job_title = '//*[@id="job"]';
    public static $value_cbb_job_title = '//*[@id="job"]//*[@label="QC Engineer"]';
    public static $cbb_level = '//*[@id="level"]';
    public static $value_cbb_level = '//*[@id="level"]//*[@label="1"]';
    public static $cbb_line_manager = '//*[@id="line"]';
    public static $value_cbb_line_manager = '//*[@id="line"]//*[@label="admin admin (LMHai)"]';
    public static $cbb_assign = '//*[@id="assign"]';
    public static $value_cbb_assign = '//*[@id="assign"]//*[@label="Employee"]';
    // Date of birth
    public static $click_input_date_of_birth = '//*[@name="bdate"]';
    public static $click_input_year_date_of_birth = '//*[@class="btn btn-default btn-sm uib-title"]';
    public static $click_year_date_of_birth = '//*[@class="btn btn-default btn-sm uib-title"]';
    public static $click_btn_previous = '//*[@class="btn btn-default btn-sm pull-left uib-left"]';
    // Chosen year
    public static $date_year = '(//*[@class="uib-year text-center ng-scope"])[14]';
    // Chosen month
    public static $date_month = '(//*[@class="uib-month text-center ng-scope"])[12]';
    // Chosen day
    public static $date_day = '(//*[@class="uib-day text-center ng-scope"])[6]';
    // Company-join Date
    public static $click_company_join= '//*[@name="cdate"]';
    public static $click_btn_company_join_today = '//*[@class="btn btn-sm btn-info uib-datepicker-current ng-binding"]';
    // Create button
    public static $btn_create_employee = '//*[@id="new-employee"]//*[@id="btnConfirm"]';
    // Message text notification
    public static $message_notification = '//span[@data-notify = "message"]';

    // Data
    public static $username;
    public static $firstname;
    public static $lastname;
    public static $email;
    public static $address;
}
