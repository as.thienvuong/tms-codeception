<?php
namespace Page;

class SearchProjectPage
{
    // include url of current page
    public static $URL = '';

    /**
     * Declare UI map for this page here. CSS or XPath allowed.
     * public static $usernameField = '#username';
     * public static $formSubmitButton = "#mainForm input[type=submit]";
     */

    /**
     * Basic route example for your current URL
     * You can append any additional parameter to URL
     * and use it in tests like: Page\Edit::route('/123-post');
     */
    public static function route($param)
    {
        return static::$URL.$param;
    }

    public static $drop_down_list_project = '(//a[@class="dropdown-toggle"])[2]';
    public static $select_create_project = '(//ul[@class="dropdown-menu"])[2]//li[1]';
    public static $select_search_project = '(//ul[@class="dropdown-menu"])[2]//li[2]';

    public static $txt_project_name = '//*[@ng-model="input.projectname"]';
    public static $btn_search_project = '//search-project//*[@class="btn btn-default glyphicon glyphicon-search"]';
}
