<?php
namespace Page;

class WorkingHostoryPage
{
    // include url of current page
    public static $URL = '';

    /**
     * Declare UI map for this page here. CSS or XPath allowed.
     * public static $usernameField = '#username';
     * public static $formSubmitButton = "#mainForm input[type=submit]";
     */

    /**
     * Basic route example for your current URL
     * You can append any additional parameter to URL
     * and use it in tests like: Page\Edit::route('/123-post');
     */
    public static function route($param)
    {
        return static::$URL.$param;
    }

    public static $txt_projectname = '//input[@id="projectname"]';
    public static $btn_save_working_history = '//button[@class="btn btn-primary"]';

    public static $message_notification = '//span[@data-notify = "message"]';

    public static $table_working_history = '//table[@id="tbl-historic"]';



}
