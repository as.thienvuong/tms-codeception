<?php
namespace Page;


class CreateProjectPage
{
    // include url of current page
    public static $URL = '';

    /**
     * Declare UI map for this page here. CSS or XPath allowed.
     * public static $usernameField = '#username';
     * public static $formSubmitButton = "#mainForm input[type=submit]";
     */

    /**
     * Basic route example for your current URL
     * You can append any additional parameter to URL
     * and use it in tests like: Page\Edit::route('/123-post');
     */
    public static function route($param)
    {
        return static::$URL.$param;
    }
    // Text box Field
    public static $txt_project_name = '//*[@id="name"]';
    public static $txt_size_day = '//*[@id="sizeday"]';
    public  static $txt_short_description = '//*[@id="shortDescription"]';
    public static $txt_long_description = '//*[@id="longDescription"]';
    public static $txt_technologies = '//*[@id="technologies"]';
    public static $txt_client_name = '//*[@id="clientName"]';
    // Selected
    // Project type
    public static $cbb_project_type = '//select[@id="projecttype"]';
    public static $cbb_value_project_type = '//select[@id="projecttype"]//option[@label="ODC"]';
    // Project status
    public static $cbb_project_status = '//select[@id="status"]';
    public static $cbb_value_project_status = '//select[@id="status"]//option[@label="Pending"]';
    // Project Manager
    public static $cbb_project_manager = '//select[@id="projectManager"]';
    public static $cbb_value_project_manager = '//select[@id="projectManager"]//option[@label="Duc Le (PMHai)"]';
    // Delivery Manager
    public static $cbb_delivery_manager = '//select[@id="deliveryManager"]';
    public static $cbb_value_delivery_manager = '//select[@id="deliveryManager"]//option[@label="Manh Le (SMHai)"]';
    // Engagement Manager
    public static $cbb_engagement_manager = '//select[@id="engagementManager"]';
    public static $cbb_value_engagement_manager = '//select[@id="engagementManager"]//option[@label="Manh Le (SMHai)"]';
    // Date
    public static $date_start_date = '//*[@name="sdate"]';
    public static $today_date_start_date = '//*[@class="btn btn-sm btn-info uib-datepicker-current ng-binding"]';
    // Button create project
    public static $btn_create_project = '//form[@id="newProject"]//button[@id="btnConfirm"]';
    // Message
    public static $message_notification = '//span[@data-notify = "message"]';
    // Get actural value
    public static $acturalprojectname = '//div[@class="col-md-4 col-md-offset-2 ng-scope"]//div[1]//p';
    public static $acturalprojecttype = '//div[@class="col-md-4 col-md-offset-2 ng-scope"]//div[2]//p';
    public static $acturalprojectstatus = '//div[@class="col-md-4 col-md-offset-2 ng-scope"]//div[3]//p';
    public static $acturalstartdate = '//div[@class="col-md-4 col-md-offset-2 ng-scope"]//div[4]//p';
    public static $acturalsizeday = '//div[@class="col-md-4 col-md-offset-2 ng-scope"]//div[6]//p';
    public static $acturalprojectmanager = '//div[@class="col-md-4 col-md-offset-2 ng-scope"]//div[8]//p';
    public static $acturaldeliveryManager = '//div[@class="col-md-4 col-md-offset-2 ng-scope"]//div[9]//p';
    public static $acturalengagementManager = '//div[@class="col-md-4 col-md-offset-2 ng-scope"]//div[10]//p';
    public static $acturalshortdescription = '//div[@class="col-md-4 ng-scope"]//div[1]//p';
    public static $acturallongdescription = '//div[@class="col-md-4 ng-scope"]//div[2]//p';
    public static $acturaltechnologies = '//div[@class="col-md-4 ng-scope"]//div[3]//p';
    public static $acturalclientname = '//div[@class="col-md-4 ng-scope"]//div[4]//p';
    public static $start_date = '//*[@name="sdate"]';

    // Account
    public static $login_username = 'adminHai';
    public static $login_password = 'sI3uPyn1';

    // Data fields
    public static $projectname;
    public static $sizeday = 100;
    public static $shortdescription = 'This is short description';
    public static $longdescription = 'This is long description';
    public static $technologies = 'PHP, Codeception';
    public static $clientname = 'Havervey Nash';

    //Get data when fill field
    public static $projecttype;
    public static $projectstatus;
    public static $projectManager;
    public static $deliveryManager;
    public static $engagementManager;
    public static $startdate;
}

