<?php
namespace Page;


class SearchEmployeePage
{
    // include url of current page
    public static $URL = '';

    /**
     * Declare UI map for this page here. CSS or XPath allowed.
     * public static $usernameField = '#username';
     * public static $formSubmitButton = "#mainForm input[type=submit]";
     */

    /**
     * Basic route example for your current URL
     * You can append any additional parameter to URL
     * and use it in tests like: Page\Edit::route('/123-post');
     */
    public static function route($param)
    {
        return static::$URL.$param;
    }

    public static $btn_create_employee = '//*[@class="btn btn-primary"]';

    public static $txt_firstname = '//*[@class="lineheight"]//*[@name="firstname"]';
    public static $txt_lastname = '//*[@class="lineheight"]//*[@name="lastname"]';
    public static $btn_search_employee = '//search-employee//*[@class="btn btn-default glyphicon glyphicon-search"]';

    public static $table_result = '//div[@ui-view="employeesresult"]//tbody//tr';

    public static $working_email = '(//div[@class="col-md-2"]//p[@class="ng-binding"])[2]';
}
