<?php
namespace Page;

class ProfileUserPage
{
    // include url of current page
    public static $URL = '';

    /**
     * Declare UI map for this page here. CSS or XPath allowed.
     * public static $usernameField = '#username';
     * public static $formSubmitButton = "#mainForm input[type=submit]";
     */

    /**
     * Basic route example for your current URL
     * You can append any additional parameter to URL
     * and use it in tests like: Page\Edit::route('/123-post');
     */
    public static function route($param)
    {
        return static::$URL.$param;
    }

    public static $btn_edit_profile = '//a[@class="btn btn-primary"]';
    public static $tab_company_information = '(//div[@class="col-md-7"]//div[@class="panel-group"])[2]';
    public static $txt_company_email = '(//div[@class="col-md-7"]//div[@class="panel-group"])[2]//input[@id="cemail"]';
    public static $btn_save_company_information = '//div[@id="collapse2"]//button[@class="btn btn-primary"]';
    public static $message_notification = '//span[@data-notify = "message"]';
    public static $btn_view_mode = '//div[@id="commands"]';
    public static $drop_down_list_profile = '//*[@class="dropdown-toggle ng-binding"]';
    public static $btn_logout = '(//*[@class="dropdown-menu"])[3]//li[2]';
    public static $btn_profile = '(//*[@class="dropdown-menu"])[3]//li[1]';
    public static $link_login_again = '//div[@class="col-md-11"]//a';

    public static $label_first_name = '(//div[@id="collapse1"]//div[@class="col-md-6"]//p)[1]';
    public static $label_last_name = '(//div[@id="collapse1"]//div[@class="col-md-6"]//p)[7]';
    public static $label_company_email = '(//div[@class="col-md-2"]//p[@class="ng-binding"])[2]';

    public static $assign_tab = '//ul[@class="nav nav-tabs"]//li[2]';
    public static $label_total_working_history = '//div[@id="content-historic"]//span[@class="text-muted ng-binding"]';
    public static $btn_add_more = '//div[@id="content-historic"]//button[@class="btn btn-primary"]';
    // Data
    public static $email;
    public static $user_first_name;
    public static $user_last_name;
    public static $email_user;
    public static $total_working_history;
    public static $total_working_history_change;
    public static $emailActural;
    public static $total_working_history_user_change;

}
