<?php
namespace Step\Acceptance;

use Page\CreateProjectPage,
    Page\DetailProjectPage as DetailProject; 

class DetailProjectStep extends \AcceptanceTester
{
    public function editProjectName(\AcceptanceTester $I)
    {
        CreateProjectPage::$projectname = 'TMSProjectEdit.'.$I->generateRandomString(6);
        // Click on edit button
        $I->seeElement(DetailProject::$btn_edit_project);
        $I->click(DetailProject::$btn_edit_project);
        // Fill text field and click button save
        $I->fillField(DetailProject::$txt_project_name,CreateProjectPage::$projectname);
        $I->seeElement(DetailProject::$btn_save_edit_project);
        $I->click(DetailProject::$btn_save_edit_project);

    }

    public function checkMessageShownCorrect(\AcceptanceTester $I)
    {
        $I->see('Edit project sucessfully',DetailProject::$message_notification);
        $I->waitForElementNotVisible(DetailProject::$message_notification, 10);
    }
}