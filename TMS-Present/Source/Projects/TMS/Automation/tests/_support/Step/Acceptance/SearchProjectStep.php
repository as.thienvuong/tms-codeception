<?php
namespace Step\Acceptance;

use Page\CreateProjectPage,
    Page\SearchProjectPage as SearchProject;

class SearchProjectStep extends \AcceptanceTester
{
    public function searchProjectPage(\AcceptanceTester $I)
    {
        $I->seeElement(SearchProject::$drop_down_list_project);
        $I->click(SearchProject::$drop_down_list_project);
        $I->seeElement(SearchProject::$select_search_project);
        $I->click(SearchProject::$select_search_project);

    }

    public function fillFieldSearchProject(\AcceptanceTester $I)
    {
        $I->seeElement(SearchProject::$txt_project_name);
        $I->fillField(SearchProject::$txt_project_name, CreateProjectPage::$projectname);
    }

    public function clickButtonSearchProjectPage(\AcceptanceTester $I)
    {
        $I->waitForElementVisible(SearchProject::$btn_search_project);
        $I->click(SearchProject::$btn_search_project);
    }
}