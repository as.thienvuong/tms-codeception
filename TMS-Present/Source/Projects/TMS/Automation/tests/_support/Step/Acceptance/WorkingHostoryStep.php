<?php
namespace Step\Acceptance;

use Page\WorkingHostoryPage as WorkingHostory; 

class WorkingHostoryStep extends \AcceptanceTester
{
    public function fillFieldsWorkingHistory(\AcceptanceTester $I)
    {
        $I->seeElement(WorkingHostory::$txt_projectname);
        $I->fillField(WorkingHostory::$txt_projectname,'Testing working history ');
    }

    public function clickOnButtonSaveWorkingHistory(\AcceptanceTester $I)
    {
        $I->seeElement(WorkingHostory::$btn_save_working_history);
        $I->click(WorkingHostory::$btn_save_working_history);
    }

    public function checkMessageNotificationShownCorrect(\AcceptanceTester $I)
    {
        $I->see('Add new Working History successfully !',WorkingHostory::$message_notification);
        $I->waitForElementNotVisible(WorkingHostory::$message_notification, 10);
    }
}