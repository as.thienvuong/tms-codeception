<?php
namespace Step\Acceptance;

use Page\SearchProjectPage as SearchProject,
    Page\CreateProjectPage as CreateProject;

class CreateProjectStep extends \AcceptanceTester
{
    public function clickCreateProjectButton(\AcceptanceTester $I)
    {
        $I->seeElement(SearchProject::$drop_down_list_project);
        $I->click(SearchProject::$drop_down_list_project);
        $I->seeElement(SearchProject::$select_create_project);
        $I->click(SearchProject::$select_create_project);
    }

    public function fillField(\AcceptanceTester $I)
    {
        CreateProject::$projectname = 'TMSProject.'.$I->generateRandomString(6);
        $I->seeElement(CreateProject::$txt_project_name);
        $I->fillField(CreateProject::$txt_project_name, CreateProject::$projectname);
        $I->seeElement(CreateProject::$txt_size_day);
        $I->fillField(CreateProject::$txt_size_day, CreateProject::$sizeday);
        $I->seeElement(CreateProject::$txt_short_description);
        $I->fillField(CreateProject::$txt_short_description, CreateProject::$shortdescription);
        $I->seeElement(CreateProject::$txt_long_description);
        $I->fillField(CreateProject::$txt_long_description, CreateProject::$longdescription);
        $I->seeElement(CreateProject::$txt_technologies);
        $I->fillField(CreateProject::$txt_technologies, CreateProject::$technologies);
        $I->seeElement(CreateProject::$txt_client_name);
        $I->fillField(CreateProject::$txt_client_name, CreateProject::$clientname);
    }

    public function selectedCombobox(\AcceptanceTester $I)
    {
        // Selected project type
        $I->seeElement(CreateProject::$cbb_project_type);
        $I->click(CreateProject::$cbb_project_type);
        $I->seeElement(CreateProject::$cbb_value_project_type);
        $I->click(CreateProject::$cbb_value_project_type);
        CreateProject::$projecttype = $I->grabTextFrom(CreateProject::$cbb_value_project_type);

        // Selected status
        $I->seeElement(CreateProject::$cbb_project_status);
        $I->click(CreateProject::$cbb_project_status);
        $I->seeElement(CreateProject::$cbb_value_project_status);
        $I->click(CreateProject::$cbb_value_project_status);
        CreateProject::$projectstatus = $I->grabTextFrom(CreateProject::$cbb_value_project_status);

        // Selected project manager
        $I->seeElement(CreateProject::$cbb_project_manager);
        $I->click(CreateProject::$cbb_project_manager);
        $I->seeElement(CreateProject::$cbb_value_project_manager);
        $I->click(CreateProject::$cbb_value_project_manager);
        CreateProject::$projectManager = $I->grabTextFrom(CreateProject::$cbb_value_project_manager);

        // Selected delivery manager
        $I->seeElement(CreateProject::$cbb_delivery_manager);
        $I->click(CreateProject::$cbb_delivery_manager);
        $I->seeElement(CreateProject::$cbb_value_delivery_manager);
        $I->click(CreateProject::$cbb_value_delivery_manager);
        CreateProject::$deliveryManager = $I->grabTextFrom(CreateProject::$cbb_value_delivery_manager);

        // Selected engagement manager
        $I->seeElement(CreateProject::$cbb_engagement_manager);
        $I->click(CreateProject::$cbb_engagement_manager);
        $I->seeElement(CreateProject::$cbb_value_engagement_manager);
        $I->click(CreateProject::$cbb_value_engagement_manager);
        CreateProject::$engagementManager = $I->grabTextFrom(CreateProject::$cbb_value_engagement_manager);
    }

    public function startDate(\AcceptanceTester $I)
    {
        $I->seeElement(CreateProject::$date_start_date);
        $I->click(CreateProject::$date_start_date);
        $I->seeElement(CreateProject::$today_date_start_date);
        $I->click(CreateProject::$today_date_start_date);
        CreateProject::$startdate = $I->grabValueFrom(CreateProject::$start_date);
    }

    public function clickButtonCreateNewProject(\AcceptanceTester $I)
    {
        $I->seeElement(CreateProject::$btn_create_project);
        $I->click(CreateProject::$btn_create_project);
    }

    public function checkMessageShownCorrect(\AcceptanceTester $I)
    {
        $I->waitForElementVisible(CreateProject::$message_notification);
        $I->see('Add project sucessfully',CreateProject::$message_notification);
        $I->waitForElementNotVisible(CreateProject::$message_notification, 10);
    }

    public function compareInputField(\AcceptanceTester $I)
    {
        $I->see(CreateProject::$projectname,CreateProject::$acturalprojectname);
        $I->see(CreateProject::$projecttype,CreateProject::$acturalprojecttype);
        $I->see(CreateProject::$projectstatus,CreateProject::$acturalprojectstatus);
        $I->see(CreateProject::$startdate,CreateProject::$acturalstartdate);
        $I->see(CreateProject::$sizeday,CreateProject::$acturalsizeday);

        $I->assertContains($I->grabTextFrom(CreateProject::$acturalprojectmanager),CreateProject::$projectManager);
        $I->assertContains($I->grabTextFrom(CreateProject::$acturaldeliveryManager),CreateProject::$deliveryManager);
        $I->assertContains($I->grabTextFrom(CreateProject::$acturalengagementManager),CreateProject::$engagementManager);

        $I->see(CreateProject::$shortdescription,CreateProject::$acturalshortdescription);
        $I->see(CreateProject::$longdescription,CreateProject::$acturallongdescription);
        $I->see(CreateProject::$technologies,CreateProject::$acturaltechnologies);
        $I->see(CreateProject::$clientname,CreateProject::$acturalclientname);
    }
}