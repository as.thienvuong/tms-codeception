<?php
namespace Step\Acceptance;

use Page\CreateEmployeePage,
    Page\ProfileUserPage as ProfileUser,
    Page\WorkingHostoryPage as WorkingHostory;

class ProfileUserStep extends \AcceptanceTester
{
    public function clickOnEditButton(\AcceptanceTester $I)
    {
        $I->seeElement(ProfileUser::$btn_edit_profile);
        $I->click(ProfileUser::$btn_edit_profile);
    }

    public function clickOnEditCompannyInformation(\AcceptanceTester $I)
    {
        $I->seeElement(ProfileUser::$tab_company_information);
        $I->click(ProfileUser::$tab_company_information);
    }

    public function editCompanyEmail(\AcceptanceTester $I)
    {
        CreateEmployeePage::$email = 'auto.'.$I->generateRandomString(6);
        $I->seeElement(ProfileUser::$txt_company_email);
        $I->fillField(ProfileUser::$txt_company_email,CreateEmployeePage::$email);
    }

    public function clickOnSaveButton(\AcceptanceTester $I)
    {
        $I->seeElement(ProfileUser::$btn_save_company_information);
        $I->click(ProfileUser::$btn_save_company_information);
    }

    public function checkMessageShownCorrect(\AcceptanceTester $I)
    {
        $I->see('Update company information successfully..',ProfileUser::$message_notification);
        $I->waitForElementNotVisible(ProfileUser::$message_notification, 10);
    }

    public function clickOnViewMode(\AcceptanceTester $I)
    {
        $I->seeElement(ProfileUser::$btn_view_mode);
        $I->click(ProfileUser::$btn_view_mode);
    }

    public function chooseProfile(\AcceptanceTester $I)
    {
        $I->seeElement(ProfileUser::$drop_down_list_profile);
        $I->click(ProfileUser::$drop_down_list_profile);
    }

    public function chooseLogout(\AcceptanceTester $I)
    {
        $I->seeElement(ProfileUser::$btn_logout);
        $I->click(ProfileUser::$btn_logout);
    }

    public function clickLinkLogout(\AcceptanceTester $I)
    {
        $I->seeElement(ProfileUser::$btn_logout);
        $I->click(ProfileUser::$btn_logout);
    }

    public function clickOnLoginAgain(\AcceptanceTester $I)
    {
        $I->seeElement(ProfileUser::$link_login_again);
        $I->click(ProfileUser::$link_login_again);
    }

    public function chooseButtonProfile(\AcceptanceTester $I)
    {
        $I->seeElement(ProfileUser::$btn_profile);
        $I->click(ProfileUser::$btn_profile);
    }

    public function getInfomationAccount(\AcceptanceTester $I)
    {
        ProfileUser::$user_first_name = $I->grabTextFrom(ProfileUser::$label_first_name);
        ProfileUser::$user_first_name = substr(ProfileUser::$user_first_name,12,strlen(ProfileUser::$user_first_name));
        ProfileUser::$user_last_name = $I->grabTextFrom(ProfileUser::$label_last_name);
        ProfileUser::$user_last_name = substr(ProfileUser::$user_last_name,11,strlen(ProfileUser::$user_last_name));
        ProfileUser::$email_user = $I->grabTextFrom(ProfileUser::$label_company_email);
    }

    public function clickOnAssignmentTab(\AcceptanceTester $I)
    {
        $I->seeElement(ProfileUser::$assign_tab);
        $I->click(ProfileUser::$assign_tab);
    }

    public function getTotalWorkingHistory(\AcceptanceTester $I)
    {
        ProfileUser::$total_working_history = $I->grabTextFrom(ProfileUser::$label_total_working_history);
        ProfileUser::$total_working_history = substr(ProfileUser::$total_working_history,26,strlen(ProfileUser::$total_working_history));
    }

    public function clickOnButtonAddMore(\AcceptanceTester $I)
    {
        $I->seeElement(ProfileUser::$btn_add_more);
        $I->click(ProfileUser::$btn_add_more);
    }

    public function compareTotalWorkingHistory(\AcceptanceTester $I)
    {
        $I->waitForElementVisible(WorkingHostory::$table_working_history);
        ProfileUser::$total_working_history_change = $I->grabTextFrom(ProfileUser::$label_total_working_history);
        ProfileUser::$total_working_history_change = substr(ProfileUser::$total_working_history_change,26,strlen(ProfileUser::$total_working_history_change));
        $I->assertEquals(ProfileUser::$total_working_history+1,(int)ProfileUser::$total_working_history_change);
    }

    public function checkEmailCorrect(\AcceptanceTester $I)
    {
        ProfileUser::$emailActural = $I->grabTextFrom(ProfileUser::$label_company_email);
        $I->assertEquals(ProfileUser::$email_user, ProfileUser::$emailActural);
    }


}