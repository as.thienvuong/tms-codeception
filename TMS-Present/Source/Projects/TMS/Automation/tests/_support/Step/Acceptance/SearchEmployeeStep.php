<?php
namespace Step\Acceptance;

use Page\CreateEmployeePage as CreateEmployee,
    Page\SearchEmployeePage as SearchEmployee;

class SearchEmployeeStep extends \AcceptanceTester
{

    public function clickCreateEmployee(\AcceptanceTester $I)
    {
        $I->click(SearchEmployee::$btn_create_employee);
    }

    public function searchEmployee(\AcceptanceTester $I)
    {
        $I->seeElement(SearchEmployee::$txt_firstname);
        $I->fillField(SearchEmployee::$txt_firstname, CreateEmployee::$firstname);
        $I->seeElement(SearchEmployee::$txt_lastname);
        $I->fillField(SearchEmployee::$txt_lastname, CreateEmployee::$lastname);
    }

    public function clickSearchEmployeeButton(\AcceptanceTester $I)
    {
        $I->click(SearchEmployee::$btn_search_employee);
    }
}