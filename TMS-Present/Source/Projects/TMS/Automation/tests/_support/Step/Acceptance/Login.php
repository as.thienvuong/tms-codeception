<?php
namespace Step\Acceptance;

use _generated\AcceptanceTesterActions;
use Page\Login as LoginPage;
use Page\ProfileUserPage as ProfileUser;
use Page\TestingOOP;


class Login extends \AcceptanceTester
{

    public function login(\AcceptanceTester $I)
    {
        $I->amOnPage('/');
        $I->fillField(LoginPage::$txt_username, LoginPage::$login_username);
        $I->fillField(LoginPage::$txt_password, LoginPage::$login_password);
        $I->click(LoginPage::$btn_search);
    }

    public function logout(\AcceptanceTester $I)
    {
        $I->seeElement(ProfileUser::$drop_down_list_profile);
        $I->click(ProfileUser::$drop_down_list_profile);
        $I->seeElement(ProfileUser::$btn_logout);
        $I->click(ProfileUser::$btn_logout);
        $I->seeElement(ProfileUser::$link_login_again);
        $I->click(ProfileUser::$link_login_again);
    }
}