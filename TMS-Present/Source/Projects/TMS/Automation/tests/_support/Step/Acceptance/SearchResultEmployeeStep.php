<?php
namespace Step\Acceptance;

use Page\CreateEmployeePage as CreateEmployee;
use Page\ProfileUserPage as Profile;
use Page\ProfileUserPage as ProfileUser,
    Page\SearchResultEmployeePage as SearchResultEmployee;

class SearchResultEmployeeStep extends \AcceptanceTester
{
    public function checkThatEmployeeWasCreated(\AcceptanceTester $I)
    {
        $I->waitForElementVisible(SearchResultEmployee::$table_result_employee);
        $I->waitForElementVisible(SearchResultEmployee::$first_result_employee);
        $I->seeElement(SearchResultEmployee::$first_result_employee);
        $I->click(SearchResultEmployee::$first_result_employee);
        $I->see("Working email: " .CreateEmployee::$email . "@nashtechglobal.com",Profile::$label_company_email);
    }

    public function clickOnFirstResult(\AcceptanceTester $I)
    {
        $I->seeElement(SearchResultEmployee::$first_result_employee);
        $I->click(SearchResultEmployee::$first_result_employee);
    }

    public function checkEmailChangeCorrect(\AcceptanceTester $I)
    {
        $I->waitForElementVisible(SearchResultEmployee::$table_result_employee);
        $I->seeElement(SearchResultEmployee::$first_result_employee);
        $I->click(SearchResultEmployee::$first_result_employee);
//        $emailActural = $I->grabTextFrom(ProfileUser::$label_company_email);
//        $I->assertEquals("Working email: ". CreateEmployee::$email ."@nashtechglobal.com", $emailActural);
        $I->see("Working email: ". CreateEmployee::$email ."@nashtechglobal.com",ProfileUser::$label_company_email);
    }
}