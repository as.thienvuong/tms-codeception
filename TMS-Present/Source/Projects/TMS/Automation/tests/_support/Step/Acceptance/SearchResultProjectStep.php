<?php
namespace Step\Acceptance;

use Page\CreateProjectPage,
    Page\SearchResultProjectPage as SearchResultProject;

class SearchResultProjectStep extends \AcceptanceTester
{
    public function checkThatProjecteWasCreated(\AcceptanceTester $I)
    {
        $I->waitForElementVisible(SearchResultProject::$table_result_project);
        $I->seeElement(SearchResultProject::$first_result_project);
        $I->see(CreateProjectPage::$projectname,SearchResultProject::$first_result_project);
    }

    public function choseFirstProject(\AcceptanceTester $I)
    {
        $I->seeElement(SearchResultProject::$first_result_project);
        $I->click(SearchResultProject::$first_result_project);
    }
}