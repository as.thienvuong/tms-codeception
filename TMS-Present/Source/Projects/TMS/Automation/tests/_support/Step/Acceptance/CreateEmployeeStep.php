<?php
namespace Step\Acceptance;

use Page\CreateEmployeePage as CreateEmployee;

class CreateEmployeeStep extends \AcceptanceTester
{
    public function fillField(\AcceptanceTester $I)
    {

        CreateEmployee::$username =  $I->generateRandomString(6);
        CreateEmployee::$firstname = 'Lam'.$I->generateRandomString(6);
        CreateEmployee::$lastname = 'Hung'.$I->generateRandomString(6);
        CreateEmployee::$email = 'auto.'.$I->generateRandomString(6);
        CreateEmployee::$address = '312 Kinh Dương Vương, P.An Lạc, Q.Bình Tân';
        // Fill fields
        $I->seeElement(CreateEmployee::$txt_username);
        $I->fillField(CreateEmployee::$txt_username,CreateEmployee::$username);
        $I->seeElement(CreateEmployee::$txt_firstname);
        $I->fillField(CreateEmployee::$txt_firstname,CreateEmployee::$firstname);
        $I->seeElement(CreateEmployee::$txt_lastname);
        $I->fillField(CreateEmployee::$txt_lastname,CreateEmployee::$lastname);
        $I->seeElement(CreateEmployee::$txt_address);
        $I->fillField(CreateEmployee::$txt_address,CreateEmployee::$address);
        $I->seeElement(CreateEmployee::$txt_email);
        $I->fillField(CreateEmployee::$txt_email, CreateEmployee::$email);
    }
    public function chooseGender(\AcceptanceTester $I)
    {
        // Choose gender
        $I->seeElement(CreateEmployee::$rdb_gender);
        $I->click(CreateEmployee::$rdb_gender);
    }
    public function selectedCombobox(\AcceptanceTester $I)
    {
        // Selected status
        $I->seeElement(CreateEmployee::$cbb_status);
        $I->click(CreateEmployee::$cbb_status);
        $I->seeElement(CreateEmployee::$value_cbb_status);
        $I->click(CreateEmployee::$value_cbb_status);

        //  Selected department
        $I->seeElement(CreateEmployee::$cbb_department);
        $I->click(CreateEmployee::$cbb_department);
        $I->seeElement(CreateEmployee::$value_cbb_department);
        $I->click(CreateEmployee::$value_cbb_department);

        // Selected competence
        $I->seeElement(CreateEmployee::$cbb_competence);
        $I->click(CreateEmployee::$cbb_competence);
        $I->seeElement(CreateEmployee::$value_cbb_competence);
        $I->click(CreateEmployee::$value_cbb_competence);

        // Selected job title
        $I->seeElement(CreateEmployee::$cbb_job_title);
        $I->click(CreateEmployee::$cbb_job_title);
        $I->seeElement(CreateEmployee::$value_cbb_job_title);
        $I->click(CreateEmployee::$value_cbb_job_title);

        // Selected level
        $I->seeElement(CreateEmployee::$cbb_level);
        $I->click(CreateEmployee::$cbb_level);
        $I->seeElement(CreateEmployee::$value_cbb_level);
        $I->click(CreateEmployee::$value_cbb_level);

        // Selected line manager
        $I->seeElement(CreateEmployee::$cbb_line_manager);
        $I->click(CreateEmployee::$cbb_line_manager);
        $I->seeElement(CreateEmployee::$value_cbb_line_manager);
        $I->click(CreateEmployee::$value_cbb_line_manager);

        // Selected assign
        $I->seeElement(CreateEmployee::$cbb_assign);
        $I->click(CreateEmployee::$cbb_assign);
        $I->seeElement(CreateEmployee::$value_cbb_assign);
        $I->click(CreateEmployee::$value_cbb_assign);
    }
    public function chooseDateOfBirth(\AcceptanceTester $I)
    {
        // Date of birth
        $I->seeElement(CreateEmployee::$click_input_date_of_birth);
        $I->click(CreateEmployee::$click_input_date_of_birth);
        $I->seeElement(CreateEmployee::$click_input_year_date_of_birth);
        $I->click(CreateEmployee::$click_input_year_date_of_birth);
        $I->seeElement(CreateEmployee::$click_year_date_of_birth);
        $I->click(CreateEmployee::$click_year_date_of_birth);
        $I->seeElement(CreateEmployee::$click_btn_previous);
        $I->click(CreateEmployee::$click_btn_previous);
        // Choose year
        $I->seeElement(CreateEmployee::$date_year);
        $I->click(CreateEmployee::$date_year);
        // Choose month
        $I->seeElement(CreateEmployee::$date_month);
        $I->click(CreateEmployee::$date_month);
        // Choose day
        $I->seeElement(CreateEmployee::$date_day);
        $I->click(CreateEmployee::$date_day);

        // Date company-join date
        $I->seeElement(CreateEmployee::$click_company_join);
        $I->click(CreateEmployee::$click_company_join);
        $I->seeElement(CreateEmployee::$click_btn_company_join_today);
        $I->click(CreateEmployee::$click_btn_company_join_today);
    }
    public function clickCreateEmployeeButton(\AcceptanceTester $I)
    {
        $I->click(CreateEmployee::$btn_create_employee);
    }
    public function checkMessageIsShownCorrect(\AcceptanceTester $I)
    {
        $I->see('Add employee successfully !',CreateEmployee::$message_notification);
        $I->waitForElementNotVisible(CreateEmployee::$message_notification);
    }
}