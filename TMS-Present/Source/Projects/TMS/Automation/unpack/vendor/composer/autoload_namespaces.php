<?php



$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
'phpDocumentor' => array($vendorDir . '/phpdocumentor/reflection-docblock/src'),
'Psr\\Log\\' => array($vendorDir . '/psr/log'),
'Prophecy\\' => array($vendorDir . '/phpspec/prophecy/src'),
'Pheanstalk' => array($vendorDir . '/pda/pheanstalk/classes'),
'Flow\\JSONPath\\Test' => array($vendorDir . '/flow/jsonpath/tests'),
'Flow\\JSONPath' => array($vendorDir . '/flow/jsonpath/src'),
'Codeception\\' => array($vendorDir . '/codeception/specify/src'),
);
