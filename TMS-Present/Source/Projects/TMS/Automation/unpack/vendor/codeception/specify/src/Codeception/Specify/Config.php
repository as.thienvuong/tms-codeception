<?php
namespace Codeception\Specify;











class Config
{
protected static $ignoredClasses = [
'Codeception\Actor',
'Symfony\Component\EventDispatcher\EventDispatcher',
'Codeception\Scenario',
'Codeception\Lib\Parser'
];

protected static $ignoredProperties = [


 'backupGlobals',
'backupGlobalsBlacklist',
'backupStaticAttributes',
'backupStaticAttributesBlacklist',
'runTestInSeparateProcess',
'preserveGlobalState',


 'dependencies',
'dependencyInput',
'tester',
'guy',
'name'
];

protected static $deepClone = true;

public $is_deep = true;
public $ignore = array();
public $ignore_classes = array();
public $shallow = array();
public $deep = array();
public $only = null;

public function propertyIgnored($property)
{
if ($this->only) {
return !in_array($property, $this->only);
}
return in_array($property, $this->ignore);
}

public function classIgnored($value)
{
if (!is_object($value)) return false;
return in_array(get_class($value), $this->ignore_classes);
}

public function propertyIsShallowCloned($property)
{
if ($this->only and !$this->is_deep) {
return in_array($property, $this->only);
}
if (!$this->is_deep and !in_array($property, $this->deep)) {
return true;
}
return in_array($property, $this->shallow);
}

public function propertyIsDeeplyCloned($property)
{
if ($this->only and $this->is_deep) {
return in_array($property, $this->only);
}
if ($this->is_deep and !in_array($property, $this->shallow)) {
return true;
}
return in_array($property, $this->deep);
}







public static function setDeepClone($deepClone)
{
self::$deepClone = $deepClone;
}






public static function setIgnoredClasses($ignoredClasses)
{
self::$ignoredClasses = $ignoredClasses;
}











public static function setIgnoredProperties($ignoredProperties)
{
self::$ignoredProperties = $ignoredProperties;
}












public static function addIgnoredClasses($ignoredClasses)
{
self::$ignoredClasses = array_merge(self::$ignoredClasses, $ignoredClasses);
}

private function __construct()
{
}




static function create()
{
$config = new Config();
$config->is_deep = self::$deepClone;
$config->ignore = self::$ignoredProperties;
$config->ignore_classes = self::$ignoredClasses;
$config->shallow = array();
$config->deep = array();
$config->only = null;
return $config;
}
}