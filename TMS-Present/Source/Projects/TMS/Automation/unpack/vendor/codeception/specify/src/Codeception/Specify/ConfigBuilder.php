<?php
namespace Codeception\Specify;
















class ConfigBuilder
{



protected $config;

public function __construct(Config $config = null)
{
$this->config = $config;
if (!$config) {
$this->config = Config::create();
}
}


















public function ignore($properties = array())
{
if (!is_array($properties)) {
$properties = func_get_args();
}
$this->config->ignore = array_merge($this->config->ignore, $properties);
return $this;
}







public function ignoreClasses($classes = array())
{
$this->config->ignore_classes = array_merge($this->config->ignore_classes, $classes);
return $this;
}




























public function deepClone($properties = true)
{
if (is_bool($properties)) {
$this->config->is_deep = $properties;
return $this;
}
if (!is_array($properties)) {
$properties = func_get_args();
}
$this->config->deep = $properties;
return $this;
}




























public function shallowClone($properties = true)
{
if (is_bool($properties)) {
$this->config->is_deep = !$properties;
return $this;
}
if (!is_array($properties)) {
$properties = func_get_args();
}
$this->config->shallow = array_merge($this->config->shallow, $properties);
return $this;
}













public function cloneOnly($properties)
{
if (!is_array($properties)) {
$properties = func_get_args();
}
$this->config->only = $properties;
return $this;
}
} 