<?php

namespace DeepCopy\Reflection;

class ReflectionHelper
{











public static function getProperties(\ReflectionClass $ref)
{
$props = $ref->getProperties();
$propsArr = array();

foreach ($props as $prop) {
$f = $prop->getName();
$propsArr[$f] = $prop;
}

if ($parentClass = $ref->getParentClass()) {
$parentPropsArr = self::getProperties($parentClass);
if (count($parentPropsArr) > 0) {
$propsArr = array_merge($parentPropsArr, $propsArr);
}
}
return $propsArr;
}
}
