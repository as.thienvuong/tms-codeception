<?php














class PHPUnit_Framework_MockObject_MockBuilder
{



private $testCase;




private $type;




private $methods = array();




private $mockClassName = '';




private $constructorArgs = array();




private $originalConstructor = true;




private $originalClone = true;




private $autoload = true;




private $cloneArguments = false;




private $callOriginalMethods = false;




private $proxyTarget = null;





public function __construct(PHPUnit_Framework_TestCase $testCase, $type)
{
$this->testCase = $testCase;
$this->type = $type;
}






public function getMock()
{
return $this->testCase->getMock(
$this->type,
$this->methods,
$this->constructorArgs,
$this->mockClassName,
$this->originalConstructor,
$this->originalClone,
$this->autoload,
$this->cloneArguments,
$this->callOriginalMethods,
$this->proxyTarget
);
}






public function getMockForAbstractClass()
{
return $this->testCase->getMockForAbstractClass(
$this->type,
$this->constructorArgs,
$this->mockClassName,
$this->originalConstructor,
$this->originalClone,
$this->autoload,
$this->methods,
$this->cloneArguments
);
}






public function getMockForTrait()
{
return $this->testCase->getMockForTrait(
$this->type,
$this->constructorArgs,
$this->mockClassName,
$this->originalConstructor,
$this->originalClone,
$this->autoload,
$this->methods,
$this->cloneArguments
);
}







public function setMethods($methods)
{
$this->methods = $methods;

return $this;
}







public function setConstructorArgs(array $args)
{
$this->constructorArgs = $args;

return $this;
}







public function setMockClassName($name)
{
$this->mockClassName = $name;

return $this;
}






public function disableOriginalConstructor()
{
$this->originalConstructor = false;

return $this;
}







public function enableOriginalConstructor()
{
$this->originalConstructor = true;

return $this;
}






public function disableOriginalClone()
{
$this->originalClone = false;

return $this;
}







public function enableOriginalClone()
{
$this->originalClone = true;

return $this;
}






public function disableAutoload()
{
$this->autoload = false;

return $this;
}







public function enableAutoload()
{
$this->autoload = true;

return $this;
}







public function disableArgumentCloning()
{
$this->cloneArguments = false;

return $this;
}







public function enableArgumentCloning()
{
$this->cloneArguments = true;

return $this;
}







public function enableProxyingToOriginalMethods()
{
$this->callOriginalMethods = true;

return $this;
}







public function disableProxyingToOriginalMethods()
{
$this->callOriginalMethods = false;
$this->proxyTarget = null;

return $this;
}








public function setProxyTarget($object)
{
$this->proxyTarget = $object;

return $this;
}
}
