<?php


















class PHPUnit_Framework_MockObject_InvocationMocker implements PHPUnit_Framework_MockObject_Stub_MatcherCollection, PHPUnit_Framework_MockObject_Invokable, PHPUnit_Framework_MockObject_Builder_Namespace
{



protected $matchers = array();




protected $builderMap = array();




public function addMatcher(PHPUnit_Framework_MockObject_Matcher_Invocation $matcher)
{
$this->matchers[] = $matcher;
}




public function hasMatchers()
{
foreach ($this->matchers as $matcher) {
if ($matcher->hasMatchers()) {
return true;
}
}

return false;
}





public function lookupId($id)
{
if (isset($this->builderMap[$id])) {
return $this->builderMap[$id];
}

return;
}






public function registerId($id, PHPUnit_Framework_MockObject_Builder_Match $builder)
{
if (isset($this->builderMap[$id])) {
throw new PHPUnit_Framework_Exception(
'Match builder with id <' . $id . '> is already registered.'
);
}

$this->builderMap[$id] = $builder;
}





public function expects(PHPUnit_Framework_MockObject_Matcher_Invocation $matcher)
{
return new PHPUnit_Framework_MockObject_Builder_InvocationMocker(
$this,
$matcher
);
}





public function invoke(PHPUnit_Framework_MockObject_Invocation $invocation)
{
$exception = null;
$hasReturnValue = false;

if (strtolower($invocation->methodName) == '__tostring') {
$returnValue = '';
} else {
$returnValue = null;
}

foreach ($this->matchers as $match) {
try {
if ($match->matches($invocation)) {
$value = $match->invoked($invocation);

if (!$hasReturnValue) {
$returnValue = $value;
$hasReturnValue = true;
}
}
} catch (Exception $e) {
$exception = $e;
}
}

if ($exception !== null) {
throw $exception;
}

return $returnValue;
}





public function matches(PHPUnit_Framework_MockObject_Invocation $invocation)
{
foreach ($this->matchers as $matcher) {
if (!$matcher->matches($invocation)) {
return false;
}
}

return true;
}




public function verify()
{
foreach ($this->matchers as $matcher) {
$matcher->verify();
}
}
}
