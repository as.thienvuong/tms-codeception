<?php















class PHP_CodeCoverage_Driver_PHPDBG implements PHP_CodeCoverage_Driver
{



public function __construct()
{
if (PHP_SAPI !== 'phpdbg') {
throw new PHP_CodeCoverage_Exception(
'This driver requires the PHPDBG SAPI'
);
}

if (!function_exists('phpdbg_start_oplog')) {
throw new PHP_CodeCoverage_Exception(
'This build of PHPDBG does not support code coverage'
);
}
}




public function start()
{
phpdbg_start_oplog();
}






public function stop()
{
static $fetchedLines = array();

$dbgData = phpdbg_end_oplog();

if ($fetchedLines == array()) {
$sourceLines = phpdbg_get_executable();
} else {
$newFiles = array_diff(
get_included_files(),
array_keys($fetchedLines)
);

if ($newFiles) {
$sourceLines = phpdbg_get_executable(
array('files' => $newFiles)
);
} else {
$sourceLines = array();
}
}

foreach ($sourceLines as $file => $lines) {
foreach ($lines as $lineNo => $numExecuted) {
$sourceLines[$file][$lineNo] = self::LINE_NOT_EXECUTED;
}
}

$fetchedLines = array_merge($fetchedLines, $sourceLines);

return $this->detectExecutedLines($fetchedLines, $dbgData);
}








private function detectExecutedLines(array $sourceLines, array $dbgData)
{
foreach ($dbgData as $file => $coveredLines) {
foreach ($coveredLines as $lineNo => $numExecuted) {

 
 if (isset($sourceLines[$file][$lineNo])) {
$sourceLines[$file][$lineNo] = self::LINE_EXECUTED;
}
}
}

return $sourceLines;
}
}
