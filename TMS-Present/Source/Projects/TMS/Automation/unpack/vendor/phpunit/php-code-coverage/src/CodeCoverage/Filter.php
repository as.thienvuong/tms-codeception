<?php














class PHP_CodeCoverage_Filter
{





private $blacklistedFiles = array();






private $whitelistedFiles = array();








public function addDirectoryToBlacklist($directory, $suffix = '.php', $prefix = '')
{
$facade = new File_Iterator_Facade;
$files = $facade->getFilesAsArray($directory, $suffix, $prefix);

foreach ($files as $file) {
$this->addFileToBlacklist($file);
}
}






public function addFileToBlacklist($filename)
{
$this->blacklistedFiles[realpath($filename)] = true;
}






public function addFilesToBlacklist(array $files)
{
foreach ($files as $file) {
$this->addFileToBlacklist($file);
}
}








public function removeDirectoryFromBlacklist($directory, $suffix = '.php', $prefix = '')
{
$facade = new File_Iterator_Facade;
$files = $facade->getFilesAsArray($directory, $suffix, $prefix);

foreach ($files as $file) {
$this->removeFileFromBlacklist($file);
}
}






public function removeFileFromBlacklist($filename)
{
$filename = realpath($filename);

if (isset($this->blacklistedFiles[$filename])) {
unset($this->blacklistedFiles[$filename]);
}
}








public function addDirectoryToWhitelist($directory, $suffix = '.php', $prefix = '')
{
$facade = new File_Iterator_Facade;
$files = $facade->getFilesAsArray($directory, $suffix, $prefix);

foreach ($files as $file) {
$this->addFileToWhitelist($file);
}
}






public function addFileToWhitelist($filename)
{
$this->whitelistedFiles[realpath($filename)] = true;
}






public function addFilesToWhitelist(array $files)
{
foreach ($files as $file) {
$this->addFileToWhitelist($file);
}
}








public function removeDirectoryFromWhitelist($directory, $suffix = '.php', $prefix = '')
{
$facade = new File_Iterator_Facade;
$files = $facade->getFilesAsArray($directory, $suffix, $prefix);

foreach ($files as $file) {
$this->removeFileFromWhitelist($file);
}
}






public function removeFileFromWhitelist($filename)
{
$filename = realpath($filename);

if (isset($this->whitelistedFiles[$filename])) {
unset($this->whitelistedFiles[$filename]);
}
}







public function isFile($filename)
{
if ($filename == '-' ||
strpos($filename, 'vfs://') === 0 ||
strpos($filename, 'xdebug://debug-eval') !== false ||
strpos($filename, 'eval()\'d code') !== false ||
strpos($filename, 'runtime-created function') !== false ||
strpos($filename, 'runkit created function') !== false ||
strpos($filename, 'assert code') !== false ||
strpos($filename, 'regexp code') !== false) {
return false;
}

return file_exists($filename);
}











public function isFiltered($filename)
{
if (!$this->isFile($filename)) {
return true;
}

$filename = realpath($filename);

if (!empty($this->whitelistedFiles)) {
return !isset($this->whitelistedFiles[$filename]);
}

return isset($this->blacklistedFiles[$filename]);
}






public function getBlacklist()
{
return array_keys($this->blacklistedFiles);
}






public function getWhitelist()
{
return array_keys($this->whitelistedFiles);
}







public function hasWhitelist()
{
return !empty($this->whitelistedFiles);
}







public function getBlacklistedFiles()
{
return $this->blacklistedFiles;
}







public function setBlacklistedFiles($blacklistedFiles)
{
$this->blacklistedFiles = $blacklistedFiles;
}







public function getWhitelistedFiles()
{
return $this->whitelistedFiles;
}







public function setWhitelistedFiles($whitelistedFiles)
{
$this->whitelistedFiles = $whitelistedFiles;
}
}
