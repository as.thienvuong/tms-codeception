<?php









use SebastianBergmann\Exporter\Exporter;






class PHPUnit_Framework_MockObject_Invocation_Static implements PHPUnit_Framework_MockObject_Invocation, PHPUnit_Framework_SelfDescribing
{



protected static $uncloneableExtensions = array(
'mysqli' => true,
'SQLite' => true,
'sqlite3' => true,
'tidy' => true,
'xmlwriter' => true,
'xsl' => true
);




protected static $uncloneableClasses = array(
'Closure',
'COMPersistHelper',
'IteratorIterator',
'RecursiveIteratorIterator',
'SplFileObject',
'PDORow',
'ZipArchive'
);




public $className;




public $methodName;




public $parameters;







public function __construct($className, $methodName, array $parameters, $cloneObjects = false)
{
$this->className = $className;
$this->methodName = $methodName;
$this->parameters = $parameters;

if (!$cloneObjects) {
return;
}

foreach ($this->parameters as $key => $value) {
if (is_object($value)) {
$this->parameters[$key] = $this->cloneObject($value);
}
}
}




public function toString()
{
$exporter = new Exporter;

return sprintf(
'%s::%s(%s)',
$this->className,
$this->methodName,
implode(
', ',
array_map(
array($exporter, 'shortenedExport'),
$this->parameters
)
)
);
}





protected function cloneObject($original)
{
$cloneable = null;
$object = new ReflectionObject($original);


 
 if ($object->isInternal() &&
isset(self::$uncloneableExtensions[$object->getExtensionName()])) {
$cloneable = false;
}

if ($cloneable === null) {
foreach (self::$uncloneableClasses as $class) {
if ($original instanceof $class) {
$cloneable = false;
break;
}
}
}

if ($cloneable === null && method_exists($object, 'isCloneable')) {
$cloneable = $object->isCloneable();
}

if ($cloneable === null && $object->hasMethod('__clone')) {
$method = $object->getMethod('__clone');
$cloneable = $method->isPublic();
}

if ($cloneable === null) {
$cloneable = true;
}

if ($cloneable) {
try {
return clone $original;
} catch (Exception $e) {
return $original;
}
} else {
return $original;
}
}
}
