<?php














namespace Facebook\WebDriver\Remote;

class RemoteExecuteMethod implements ExecuteMethod {

private $driver;




public function __construct(RemoteWebDriver $driver) {
$this->driver = $driver;
}






public function execute(
$command_name,
array $parameters = array()
) {
return $this->driver->execute($command_name, $parameters);
}
}
