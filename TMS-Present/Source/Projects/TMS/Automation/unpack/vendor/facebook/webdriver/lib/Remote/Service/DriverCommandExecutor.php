<?php














namespace Facebook\WebDriver\Remote\Service;

use Facebook\WebDriver\Exception\WebDriverException;
use Facebook\WebDriver\Remote\DriverCommand;
use Facebook\WebDriver\Remote\HttpCommandExecutor;
use Facebook\WebDriver\Remote\WebDriverCommand;





class DriverCommandExecutor extends HttpCommandExecutor {




private $service;

public function __construct(DriverService $service) {
parent::__construct($service->getURL());
$this->service = $service;
}









public function execute(WebDriverCommand $command, $curl_opts = array()) {
if ($command->getName() === DriverCommand::NEW_SESSION) {
$this->service->start();
}

try {
$value = parent::execute($command);
if ($command->getName() === DriverCommand::QUIT) {
$this->service->stop();
}
return $value;
} catch (\Exception $e) {
if (!$this->service->isRunning()) {
throw new WebDriverException('The driver server has died.');
}
throw $e;
}
}

}
