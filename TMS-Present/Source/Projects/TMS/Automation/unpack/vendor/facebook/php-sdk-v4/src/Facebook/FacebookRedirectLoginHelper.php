<?php






















namespace Facebook;







class FacebookRedirectLoginHelper
{




private $appId;




private $appSecret;




private $redirectUrl;




private $sessionPrefix = 'FBRLH_';




protected $state;




protected $checkForSessionStatus = true;









public function __construct($redirectUrl, $appId = null, $appSecret = null)
{
$this->appId = FacebookSession::_getTargetAppId($appId);
$this->appSecret = FacebookSession::_getTargetAppSecret($appSecret);
$this->redirectUrl = $redirectUrl;
}














public function getLoginUrl(array $scope = array(), $version = null, $displayAsPopup = false, $authType = false)
{
$version = ($version ?: FacebookRequest::GRAPH_API_VERSION);
$this->state = $this->random(16);
$this->storeState($this->state);
$params = array(
'client_id' => $this->appId,
'redirect_uri' => $this->redirectUrl,
'state' => $this->state,
'sdk' => 'php-sdk-' . FacebookRequest::VERSION,
'scope' => implode(',', $scope)
);

if (in_array($authType, array(true, 'reauthenticate', 'https'), true)) {
$params['auth_type'] = $authType === true ? 'reauthenticate' : $authType;
}

if ($displayAsPopup)
{
$params['display'] = 'popup';
}

return 'https://www.facebook.com/' . $version . '/dialog/oauth?' .
http_build_query($params, null, '&');
}









public function getReRequestUrl(array $scope = array(), $version = null)
{
$version = ($version ?: FacebookRequest::GRAPH_API_VERSION);
$this->state = $this->random(16);
$this->storeState($this->state);
$params = array(
'client_id' => $this->appId,
'redirect_uri' => $this->redirectUrl,
'state' => $this->state,
'sdk' => 'php-sdk-' . FacebookRequest::VERSION,
'auth_type' => 'rerequest',
'scope' => implode(',', $scope)
);
return 'https://www.facebook.com/' . $version . '/dialog/oauth?' .
http_build_query($params, null, '&');
}












public function getLogoutUrl(FacebookSession $session, $next)
{
if ($session->getAccessToken()->isAppSession()) {
throw new FacebookSDKException(
'Cannot generate a Logout URL with an App Session.', 722
);
}
$params = array(
'next' => $next,
'access_token' => $session->getToken()
);
return 'https://www.facebook.com/logout.php?' . http_build_query($params, null, '&');
}







public function getSessionFromRedirect()
{
if ($this->isValidRedirect()) {
$params = array(
'client_id' => FacebookSession::_getTargetAppId($this->appId),
'redirect_uri' => $this->redirectUrl,
'client_secret' =>
FacebookSession::_getTargetAppSecret($this->appSecret),
'code' => $this->getCode()
);
$response = (new FacebookRequest(
FacebookSession::newAppSession($this->appId, $this->appSecret),
'GET',
'/oauth/access_token',
$params
))->execute()->getResponse();


 $accessToken = null;
if (is_object($response) && isset($response->access_token)) {
$accessToken = $response->access_token;
} elseif (is_array($response) && isset($response['access_token'])) {
$accessToken = $response['access_token'];
}

if (isset($accessToken)) {
return new FacebookSession($accessToken);
}
}
return null;
}






protected function isValidRedirect()
{
$savedState = $this->loadState();
if (!$this->getCode() || !isset($_GET['state'])) {
return false;
}
$givenState = $_GET['state'];
$savedLen = mb_strlen($savedState);
$givenLen = mb_strlen($givenState);
if ($savedLen !== $givenLen) {
return false;
}
$result = 0;
for ($i = 0; $i < $savedLen; $i++) {
$result |= ord($savedState[$i]) ^ ord($givenState[$i]);
}
return $result === 0;
}






protected function getCode()
{
return isset($_GET['code']) ? $_GET['code'] : null;
}










protected function storeState($state)
{
if ($this->checkForSessionStatus === true
&& session_status() !== PHP_SESSION_ACTIVE) {
throw new FacebookSDKException(
'Session not active, could not store state.', 720
);
}
$_SESSION[$this->sessionPrefix . 'state'] = $state;
}










protected function loadState()
{
if ($this->checkForSessionStatus === true
&& session_status() !== PHP_SESSION_ACTIVE) {
throw new FacebookSDKException(
'Session not active, could not load state.', 721
);
}
if (isset($_SESSION[$this->sessionPrefix . 'state'])) {
$this->state = $_SESSION[$this->sessionPrefix . 'state'];
return $this->state;
}
return null;
}












public function random($bytes)
{
if (!is_numeric($bytes)) {
throw new FacebookSDKException(
'random() expects an integer'
);
}
if ($bytes < 1) {
throw new FacebookSDKException(
'random() expects an integer greater than zero'
);
}
$buf = '';

 if (!ini_get('open_basedir')
&& is_readable('/dev/urandom')) {
$fp = fopen('/dev/urandom', 'rb');
if ($fp !== FALSE) {
$buf = fread($fp, $bytes);
fclose($fp);
if($buf !== FALSE) {
return bin2hex($buf);
}
}
}

if (function_exists('mcrypt_create_iv')) {
$buf = mcrypt_create_iv($bytes, MCRYPT_DEV_URANDOM);
if ($buf !== FALSE) {
return bin2hex($buf);
}
}

while (strlen($buf) < $bytes) {
$buf .= md5(uniqid(mt_rand(), true), true); 

 }
return bin2hex(substr($buf, 0, $bytes));
}




public function disableSessionStatusCheck()
{
$this->checkForSessionStatus = false;
}

}
