<?php














namespace Facebook\WebDriver\Interactions\Internal;

use Facebook\WebDriver\WebDriverMouse;
use Facebook\WebDriver\Internal\WebDriverLocatable;




class WebDriverMouseAction {




protected $mouse;




protected $locationProvider;

public function __construct(
WebDriverMouse $mouse,
WebDriverLocatable $location_provider = null) {
$this->mouse = $mouse;
$this->locationProvider = $location_provider;
}




protected function getActionLocation() {
if ($this->locationProvider !== null) {
return $this->locationProvider->getCoordinates();
}

return null;
}




protected function moveToLocation() {
$this->mouse->mouseMove($this->locationProvider);
}
}
