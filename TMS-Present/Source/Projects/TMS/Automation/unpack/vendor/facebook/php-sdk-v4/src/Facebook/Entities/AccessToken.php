<?php






















namespace Facebook\Entities;

use Facebook\FacebookRequest;
use Facebook\FacebookRequestException;
use Facebook\FacebookSession;
use Facebook\GraphSessionInfo;





class AccessToken
{






protected $accessToken;






protected $machineId;






protected $expiresAt;








public function __construct($accessToken, $expiresAt = 0, $machineId = null)
{
$this->accessToken = $accessToken;
if ($expiresAt) {
$this->setExpiresAtFromTimeStamp($expiresAt);
}
$this->machineId = $machineId;
}






protected function setExpiresAtFromTimeStamp($timeStamp)
{
$dt = new \DateTime();
$dt->setTimestamp($timeStamp);
$this->expiresAt = $dt;
}






public function getExpiresAt()
{
return $this->expiresAt;
}






public function getMachineId()
{
return $this->machineId;
}






public function isLongLived()
{
if ($this->expiresAt) {
return $this->expiresAt->getTimestamp() > time() + (60 * 60 * 2);
}
return false;
}










public function isValid($appId = null, $appSecret = null, $machineId = null)
{
$accessTokenInfo = $this->getInfo($appId, $appSecret);
$machineId = $machineId ?: $this->machineId;
return static::validateAccessToken($accessTokenInfo, $appId, $machineId);
}













public static function validateAccessToken(GraphSessionInfo $tokenInfo,
$appId = null, $machineId = null)
{
$targetAppId = FacebookSession::_getTargetAppId($appId);

$appIdIsValid = $tokenInfo->getAppId() == $targetAppId;
$machineIdIsValid = $tokenInfo->getProperty('machine_id') == $machineId;
$accessTokenIsValid = $tokenInfo->isValid();

$accessTokenIsStillAlive = true;

 if ($tokenInfo->getExpiresAt() instanceof \DateTime) {
$accessTokenIsStillAlive = $tokenInfo->getExpiresAt()->getTimestamp() >= time();
}

return $appIdIsValid && $machineIdIsValid && $accessTokenIsValid && $accessTokenIsStillAlive;
}











public static function getAccessTokenFromCode($code, $appId = null, $appSecret = null, $machineId = null)
{
$params = array(
'code' => $code,
'redirect_uri' => '',
);

if ($machineId) {
$params['machine_id'] = $machineId;
}

return static::requestAccessToken($params, $appId, $appSecret);
}










public static function getCodeFromAccessToken($accessToken, $appId = null, $appSecret = null)
{
$accessToken = (string) $accessToken;

$params = array(
'access_token' => $accessToken,
'redirect_uri' => '',
);

return static::requestCode($params, $appId, $appSecret);
}









public function extend($appId = null, $appSecret = null)
{
$params = array(
'grant_type' => 'fb_exchange_token',
'fb_exchange_token' => $this->accessToken,
);

return static::requestAccessToken($params, $appId, $appSecret);
}












public static function requestAccessToken(array $params, $appId = null, $appSecret = null)
{
$response = static::request('/oauth/access_token', $params, $appId, $appSecret);
$data = $response->getResponse();





if (is_array($data)) {
if (isset($data['access_token'])) {
$expiresAt = isset($data['expires']) ? time() + $data['expires'] : 0;
return new static($data['access_token'], $expiresAt);
}
} elseif($data instanceof \stdClass) {
if (isset($data->access_token)) {
$expiresAt = isset($data->expires_in) ? time() + $data->expires_in : 0;
$machineId = isset($data->machine_id) ? (string) $data->machine_id : null;
return new static((string) $data->access_token, $expiresAt, $machineId);
}
}

throw FacebookRequestException::create(
$response->getRawResponse(),
$data,
401
);
}












public static function requestCode(array $params, $appId = null, $appSecret = null)
{
$response = static::request('/oauth/client_code', $params, $appId, $appSecret);
$data = $response->getResponse();

if (isset($data->code)) {
return (string) $data->code;
}

throw FacebookRequestException::create(
$response->getRawResponse(),
$data,
401
);
}













protected static function request($endpoint, array $params, $appId = null, $appSecret = null)
{
$targetAppId = FacebookSession::_getTargetAppId($appId);
$targetAppSecret = FacebookSession::_getTargetAppSecret($appSecret);

if (!isset($params['client_id'])) {
$params['client_id'] = $targetAppId;
}
if (!isset($params['client_secret'])) {
$params['client_secret'] = $targetAppSecret;
}


 
 $request = new FacebookRequest(
FacebookSession::newAppSession($targetAppId, $targetAppSecret),
'GET',
$endpoint,
$params
);
return $request->execute();
}









public function getInfo($appId = null, $appSecret = null)
{
$params = array('input_token' => $this->accessToken);

$request = new FacebookRequest(
FacebookSession::newAppSession($appId, $appSecret),
'GET',
'/debug_token',
$params
);
$response = $request->execute()->getGraphObject(GraphSessionInfo::className());


 if ($response->getExpiresAt()) {
$this->expiresAt = $response->getExpiresAt();
}

return $response;
}






public function __toString()
{
return $this->accessToken;
}






public function isAppSession()
{
return strpos($this->accessToken, '|') !== false;
}

}
