<?php














namespace Facebook\WebDriver\Support\Events;

use Facebook\WebDriver\Exception\UnsupportedOperationException;
use Facebook\WebDriver\Exception\WebDriverException;
use Facebook\WebDriver\Interactions\Touch\WebDriverTouchScreen;
use Facebook\WebDriver\JavaScriptExecutor;
use Facebook\WebDriver\WebDriver;
use Facebook\WebDriver\WebDriverBy;
use Facebook\WebDriver\WebDriverDispatcher;
use Facebook\WebDriver\WebDriverElement;
use Facebook\WebDriver\WebDriverOptions;
use Facebook\WebDriver\WebDriverTargetLocator;
use Facebook\WebDriver\WebDriverWait;

class EventFiringWebDriver implements WebDriver, JavaScriptExecutor {




protected $driver;




protected $dispatcher;





public function __construct(WebDriver $driver,
WebDriverDispatcher $dispatcher = null) {
$this->dispatcher = $dispatcher ?: new WebDriverDispatcher();
if (!$this->dispatcher->getDefaultDriver()) {
$this->dispatcher->setDefaultDriver($this);
}
$this->driver = $driver;
return $this;
}




public function getDispatcher() {
return $this->dispatcher;
}





protected function dispatch($method) {
if (!$this->dispatcher) {
return;
}

$arguments = func_get_args();
unset($arguments[0]);
$this->dispatcher->dispatch($method, $arguments);
}




public function getWebDriver() {
return $this->driver;
}





protected function newElement(WebDriverElement $element) {
return new EventFiringWebElement($element, $this->getDispatcher());
}






public function get($url) {
$this->dispatch('beforeNavigateTo', $url, $this);
try {
$this->driver->get($url);
} catch (WebDriverException $exception) {
$this->dispatchOnException($exception);
}
$this->dispatch('afterNavigateTo', $url, $this);
return $this;
}






public function findElements(WebDriverBy $by) {
$this->dispatch('beforeFindBy', $by, null, $this);
try {
$elements = array();
foreach ($this->driver->findElements($by) as $element) {
$elements[] = $this->newElement($element);
}
} catch (WebDriverException $exception) {
$this->dispatchOnException($exception);
}
$this->dispatch('afterFindBy', $by, null, $this);
return $elements;

}






public function findElement(WebDriverBy $by) {
$this->dispatch('beforeFindBy', $by, null, $this);
try {
$element = $this->newElement($this->driver->findElement($by));
} catch (WebDriverException $exception) {
$this->dispatchOnException($exception);
}
$this->dispatch('afterFindBy', $by, null, $this);
return $element;
}







public function executeScript($script, array $arguments = array()) {
if (!$this->driver instanceof JavaScriptExecutor) {
throw new UnsupportedOperationException(
'driver does not implement JavaScriptExecutor'
);
}

$this->dispatch('beforeScript', $script, $this);
try {
$result = $this->driver->executeScript($script, $arguments);
} catch (WebDriverException $exception) {
$this->dispatchOnException($exception);
}
$this->dispatch('afterScript', $script, $this);
return $result;
}







public function executeAsyncScript($script, array $arguments = array()) {
if (!$this->driver instanceof JavaScriptExecutor) {
throw new UnsupportedOperationException(
'driver does not implement JavaScriptExecutor'
);
}

$this->dispatch('beforeScript', $script, $this);
try {
$result = $this->driver->executeAsyncScript($script, $arguments);
} catch (WebDriverException $exception) {
$this->dispatchOnException($exception);
}
$this->dispatch('afterScript', $script, $this);
return $result;
}





public function close() {
try {
$this->driver->close();
return $this;
} catch (WebDriverException $exception) {
$this->dispatchOnException($exception);
}
}





public function getCurrentURL() {
try {
return $this->driver->getCurrentURL();
} catch (WebDriverException $exception) {
$this->dispatchOnException($exception);
}
}





public function getPageSource() {
try {
return $this->driver->getPageSource();
} catch (WebDriverException $exception) {
$this->dispatchOnException($exception);
}
}





public function getTitle() {
try {
return $this->driver->getTitle();
} catch (WebDriverException $exception) {
$this->dispatchOnException($exception);
}
}





public function getWindowHandle() {
try {
return $this->driver->getWindowHandle();
} catch (WebDriverException $exception) {
$this->dispatchOnException($exception);
}
}





public function getWindowHandles() {
try {
return $this->driver->getWindowHandles();
} catch (WebDriverException $exception) {
$this->dispatchOnException($exception);
}
}




public function quit() {
try {
$this->driver->quit();
} catch (WebDriverException $exception) {
$this->dispatchOnException($exception);
}
}






public function takeScreenshot($save_as = null) {
try {
return $this->driver->takeScreenshot($save_as);
} catch (WebDriverException $exception) {
$this->dispatchOnException($exception);
}
}







public function wait($timeout_in_second = 30,
$interval_in_millisecond = 250) {
try {
return $this->driver->wait($timeout_in_second, $interval_in_millisecond);
} catch (WebDriverException $exception) {
$this->dispatchOnException($exception);
}
}





public function manage() {
try {
return $this->driver->manage();
} catch (WebDriverException $exception) {
$this->dispatchOnException($exception);
}
}





public function navigate() {
try {
return new EventFiringWebDriverNavigation(
$this->driver->navigate(),
$this->getDispatcher()
);
} catch (WebDriverException $exception) {
$this->dispatchOnException($exception);
}
}





public function switchTo() {
try {
return $this->driver->switchTo();
} catch (WebDriverException $exception) {
$this->dispatchOnException($exception);
}
}





public function getTouch() {
try {
return $this->driver->getTouch();
} catch (WebDriverException $exception) {
$this->dispatchOnException($exception);
}
}

private function dispatchOnException($exception) {
$this->dispatch('onException', $exception, $this);
throw $exception;
}

public function execute($name, $params) {
try {
return $this->driver->execute($name, $params);
} catch (WebDriverException $exception) {
$this->dispatchOnException($exception);
}
}
}
