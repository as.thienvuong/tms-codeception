<?php














namespace Facebook\WebDriver;

use Facebook\WebDriver\Remote\WebDriverCommand;




interface WebDriverCommandExecutor {






public function execute(WebDriverCommand $command);
}
