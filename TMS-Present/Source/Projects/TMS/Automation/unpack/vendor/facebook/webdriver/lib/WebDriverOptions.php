<?php














namespace Facebook\WebDriver;

use Facebook\WebDriver\Remote\DriverCommand;
use Facebook\WebDriver\Remote\ExecuteMethod;
use InvalidArgumentException;




class WebDriverOptions {

protected $executor;

public function __construct(ExecuteMethod $executor) {
$this->executor = $executor;
}




















public function addCookie(array $cookie) {
$this->validate($cookie);
$this->executor->execute(
DriverCommand::ADD_COOKIE,
array('cookie' => $cookie)
);
return $this;
}






public function deleteAllCookies() {
$this->executor->execute(DriverCommand::DELETE_ALL_COOKIES);
return $this;
}







public function deleteCookieNamed($name) {
$this->executor->execute(
DriverCommand::DELETE_COOKIE,
array(':name' => $name)
);
return $this;
}








public function getCookieNamed($name) {
$cookies = $this->getCookies();
foreach ($cookies as $cookie) {
if ($cookie['name'] === $name) {
return $cookie;
}
}
return null;
}






public function getCookies() {
return $this->executor->execute(DriverCommand::GET_ALL_COOKIES);
}

private function validate(array $cookie) {
if (!isset($cookie['name']) ||
$cookie['name'] === '' ||
strpos($cookie['name'], ';') !== false) {
throw new InvalidArgumentException(
'"name" should be non-empty and does not contain a ";"');
}

if (!isset($cookie['value'])) {
throw new InvalidArgumentException(
'"value" is required when setting a cookie.');
}

if (isset($cookie['domain']) && strpos($cookie['domain'], ':') !== false) {
throw new InvalidArgumentException(
'"domain" should not contain a port:'.(string)$cookie['domain']);
}
}






public function timeouts() {
return new WebDriverTimeouts($this->executor);
}







public function window() {
return new WebDriverWindow($this->executor);
}








public function getLog($log_type) {
return $this->executor->execute(
DriverCommand::GET_LOG,
array('type' => $log_type)
);
}







public function getAvailableLogTypes() {
return $this->executor->execute(DriverCommand::GET_AVAILABLE_LOG_TYPES);
}

}
