<?php






















namespace Facebook;







class FacebookRequestException extends FacebookSDKException
{




private $statusCode;




private $rawResponse;




private $responseData;








public function __construct($rawResponse, $responseData, $statusCode)
{
$this->rawResponse = $rawResponse;
$this->statusCode = $statusCode;
$this->responseData = self::convertToArray($responseData);
parent::__construct(
$this->get('message', 'Unknown Exception'), $this->get('code', -1), null
);
}











public static function create($raw, $data, $statusCode)
{
$data = self::convertToArray($data);
if (!isset($data['error']['code']) && isset($data['code'])) {
$data = array('error' => $data);
}
$code = (isset($data['error']['code']) ? (int) $data['error']['code'] : null);

if (isset($data['error']['error_subcode'])) {
switch ($data['error']['error_subcode']) {

 case 458:
case 459:
case 460:
case 463:
case 464:
case 467:
return new FacebookAuthorizationException($raw, $data, $statusCode);
break;
}
}

switch ($code) {

 case 100:
case 102:
case 190:
return new FacebookAuthorizationException($raw, $data, $statusCode);
break;


 case 1:
case 2:
return new FacebookServerException($raw, $data, $statusCode);
break;


 case 4:
case 17:
case 341:
return new FacebookThrottleException($raw, $data, $statusCode);
break;


 case 506:
return new FacebookClientException($raw, $data, $statusCode);
break;
}


 if ($code === 10 || ($code >= 200 && $code <= 299)) {
return new FacebookPermissionException($raw, $data, $statusCode);
}


 if (isset($data['error']['type'])
and $data['error']['type'] === 'OAuthException') {
return new FacebookAuthorizationException($raw, $data, $statusCode);
}


 return new FacebookOtherException($raw, $data, $statusCode);
}









private function get($key, $default = null)
{
if (isset($this->responseData['error'][$key])) {
return $this->responseData['error'][$key];
}
return $default;
}






public function getHttpStatusCode()
{
return $this->statusCode;
}






public function getSubErrorCode()
{
return $this->get('error_subcode', -1);
}






public function getErrorType()
{
return $this->get('type', '');
}






public function getRawResponse()
{
return $this->rawResponse;
}






public function getResponse()
{
return $this->responseData;
}








private static function convertToArray($object)
{
if ($object instanceof \stdClass) {
return get_object_vars($object);
}
return $object;
}

}