<?php














namespace Facebook\WebDriver\Chrome;

use Facebook\WebDriver\Remote\DesiredCapabilities;






class ChromeOptions {




const CAPABILITY = "chromeOptions";




private $arguments = array();




private $binary = '';




private $extensions = array();




private $experimentalOptions = array();








public function setBinary($path) {
$this->binary = $path;
return $this;
}





public function addArguments(array $arguments) {
$this->arguments = array_merge($this->arguments, $arguments);
return $this;
}








public function addExtensions(array $paths) {
foreach ($paths as $path) {
$this->addExtension($path);
}
return $this;
}






public function addEncodedExtensions(array $encoded_extensions) {
foreach ($encoded_extensions as $encoded_extension) {
$this->addEncodedExtension($encoded_extension);
}
return $this;
}








public function setExperimentalOption($name, $value) {
$this->experimentalOptions[$name] = $value;
return $this;
}





public function toCapabilities() {
$capabilities = DesiredCapabilities::chrome();
$capabilities->setCapability(self::CAPABILITY, $this);
return $capabilities;
}




public function toArray() {
$options = $this->experimentalOptions;


 
 
 
 $options['binary'] = $this->binary;

if ($this->arguments) {
$options['args'] = $this->arguments;
}

if ($this->extensions) {
$options['extensions'] = $this->extensions;
}

return $options;
}








private function addExtension($path) {
$this->addEncodedExtension(base64_encode(file_get_contents($path)));
return $this;
}





private function addEncodedExtension($encoded_extension) {
$this->extensions[] = $encoded_extension;
return $this;
}
}
