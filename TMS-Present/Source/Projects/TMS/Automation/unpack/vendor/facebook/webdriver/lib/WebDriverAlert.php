<?php














namespace Facebook\WebDriver;

use Facebook\WebDriver\Remote\DriverCommand;




class WebDriverAlert {

protected $executor;

public function __construct($executor) {
$this->executor = $executor;
}






public function accept() {
$this->executor->execute(DriverCommand::ACCEPT_ALERT);
return $this;
}






public function dismiss() {
$this->executor->execute(DriverCommand::DISMISS_ALERT);
return $this;
}






public function getText() {
return $this->executor->execute(DriverCommand::GET_ALERT_TEXT);
}







public function sendKeys($value) {
$this->executor->execute(
DriverCommand::SET_ALERT_VALUE,
array('text' => $value)
);
return $this;
}

}
