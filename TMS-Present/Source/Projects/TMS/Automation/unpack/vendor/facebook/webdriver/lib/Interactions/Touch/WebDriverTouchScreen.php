<?php














namespace Facebook\WebDriver\Interactions\Touch;

use Facebook\WebDriver\WebDriverElement;




interface WebDriverTouchScreen {







public function tap(WebDriverElement $element);







public function doubleTap(WebDriverElement $element);








public function down($x, $y);









public function flick($xspeed, $yspeed);











public function flickFromElement(
WebDriverElement $element, $xoffset, $yoffset, $speed);







public function longPress(WebDriverElement $element);








public function move($x, $y);










public function scroll($xoffset, $yoffset);










public function scrollFromElement(
WebDriverElement $element, $xoffset, $yoffset);








public function up($x, $y);

}
