<?php














namespace Facebook\WebDriver\Remote;

use Facebook\WebDriver\Interactions\Internal\WebDriverCoordinates;
use Facebook\WebDriver\WebDriverMouse;




class RemoteMouse implements WebDriverMouse {




private $executor;




public function __construct(RemoteExecuteMethod $executor) {
$this->executor = $executor;
}






public function click(WebDriverCoordinates $where = null) {
$this->moveIfNeeded($where);
$this->executor->execute(DriverCommand::CLICK, array(
'button' => 0,
));
return $this;
}






public function contextClick(WebDriverCoordinates $where = null) {
$this->moveIfNeeded($where);
$this->executor->execute(DriverCommand::CLICK, array(
'button' => 2,
));
return $this;
}






public function doubleClick(WebDriverCoordinates $where = null) {
$this->moveIfNeeded($where);
$this->executor->execute(DriverCommand::DOUBLE_CLICK);
return $this;
}






public function mouseDown(WebDriverCoordinates $where = null) {
$this->moveIfNeeded($where);
$this->executor->execute(DriverCommand::MOUSE_DOWN);
return $this;
}








public function mouseMove(WebDriverCoordinates $where = null,
$x_offset = null,
$y_offset = null) {
$params = array();
if ($where !== null) {
$params['element'] = $where->getAuxiliary();
}
if ($x_offset !== null) {
$params['xoffset'] = $x_offset;
}
if ($y_offset !== null) {
$params['yoffset'] = $y_offset;
}
$this->executor->execute(DriverCommand::MOVE_TO, $params);
return $this;
}






public function mouseUp(WebDriverCoordinates $where = null) {
$this->moveIfNeeded($where);
$this->executor->execute(DriverCommand::MOUSE_UP);
return $this;
}





protected function moveIfNeeded(WebDriverCoordinates $where = null) {
if ($where) {
$this->mouseMove($where);
}
}
}
