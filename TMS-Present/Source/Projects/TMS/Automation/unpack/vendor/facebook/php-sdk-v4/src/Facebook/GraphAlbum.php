<?php






















namespace Facebook;







class GraphAlbum extends GraphObject
{





public function getId()
{
return $this->getProperty('id');
}






public function canUpload()
{
return $this->getProperty('can_upload');
}






public function getCount()
{
return $this->getProperty('count');
}






public function getCoverPhoto()
{
return $this->getProperty('cover_photo');
}






public function getCreatedTime()
{
$value = $this->getProperty('created_time');
if ($value) {
return new \DateTime($value);
}
return null;
}






public function getUpdatedTime()
{
$value = $this->getProperty('updated_time');
if ($value) {
return new \DateTime($value);
}
return null;
}






public function getDescription()
{
return $this->getProperty('description');
}






public function getFrom()
{
return $this->getProperty('from', GraphUser::className());
}






public function getLink()
{
return $this->getProperty('link');
}






public function getLocation()
{
return $this->getProperty('location');
}






public function getName()
{
return $this->getProperty('name');
}






public function getPrivacy()
{
return $this->getProperty('privacy');
}






public function getType()
{
return $this->getProperty('type');
}


}
