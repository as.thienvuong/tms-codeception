<?php














namespace Facebook\WebDriver;

use Facebook\WebDriver\Exception\NoSuchElementException;
use Facebook\WebDriver\Exception\TimeOutException;







class WebDriverWait {

protected $driver;
protected $timeout;
protected $interval;

public function __construct(
WebDriver $driver,
$timeout_in_second = null,
$interval_in_millisecond = null) {
$this->driver = $driver;
$this->timeout = $timeout_in_second ?: 30;
$this->interval = $interval_in_millisecond ?: 250;
}














public function until($func_or_ec, $message = "") {
$end = microtime(true) + $this->timeout;
$last_exception = null;

while ($end > microtime(true)) {
try {
if ($func_or_ec instanceof WebDriverExpectedCondition) {
$ret_val = call_user_func($func_or_ec->getApply(), $this->driver);
} else {
$ret_val = call_user_func($func_or_ec, $this->driver);
}
if ($ret_val) {
return $ret_val;
}
} catch (NoSuchElementException $e) {
$last_exception = $e;
}
usleep($this->interval * 1000);
}

if ($last_exception) {
throw $last_exception;
}
throw new TimeOutException($message);
}
}
