<?php














namespace Facebook\WebDriver;








class WebDriverBy {

private $mechanism;
private $value;

protected function __construct($mechanism, $value) {
$this->mechanism = $mechanism;
$this->value = $value;
}




public function getMechanism() {
return $this->mechanism;
}




public function getValue() {
return $this->value;
}








public static function className($class_name) {
return new WebDriverBy('class name', $class_name);
}







public static function cssSelector($css_selector) {
return new WebDriverBy('css selector', $css_selector);
}







public static function id($id) {
return new WebDriverBy('id', $id);
}







public static function name($name) {
return new WebDriverBy('name', $name);
}







public static function linkText($link_text) {
return new WebDriverBy('link text', $link_text);
}








public static function partialLinkText($partial_link_text) {
return new WebDriverBy('partial link text', $partial_link_text);
}







public static function tagName($tag_name) {
return new WebDriverBy('tag name', $tag_name);
}







public static function xpath($xpath) {
return new WebDriverBy('xpath', $xpath);
}
}
