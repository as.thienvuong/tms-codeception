<?php














namespace Facebook\WebDriver;




class WebDriverDimension {

private $height, $width;

public function __construct($width, $height) {
$this->width = $width;
$this->height = $height;
}






public function getHeight() {
return $this->height;
}






public function getWidth() {
return $this->width;
}








public function equals(WebDriverDimension $dimension) {
return $this->height === $dimension->getHeight() &&
$this->width === $dimension->getWidth();
}
}
