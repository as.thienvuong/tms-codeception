<?php






















namespace Facebook;






class GraphUserPage extends GraphObject
{






public function getId()
{
return $this->getProperty('id');
}






public function getCategory()
{
return $this->getProperty('category');
}






public function getName()
{
return $this->getProperty('name');
}






public function getAccessToken()
{
return $this->getProperty('access_token');
}






public function getPermissions()
{
return $this->getProperty('perms');
}

}