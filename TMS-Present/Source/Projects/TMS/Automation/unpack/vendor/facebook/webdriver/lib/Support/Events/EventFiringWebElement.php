<?php














namespace Facebook\WebDriver\Support\Events;

use Facebook\WebDriver\Exception\WebDriverException;
use Facebook\WebDriver\Interactions\Internal\WebDriverCoordinates;
use Facebook\WebDriver\Internal\WebDriverLocatable;
use Facebook\WebDriver\WebDriverBy;
use Facebook\WebDriver\WebDriverDimension;
use Facebook\WebDriver\WebDriverDispatcher;
use Facebook\WebDriver\WebDriverElement;
use Facebook\WebDriver\WebDriverPoint;

class EventFiringWebElement implements WebDriverElement, WebDriverLocatable {




protected $element;




protected $dispatcher;





public function __construct(WebDriverElement $element,
WebDriverDispatcher $dispatcher) {
$this->element = $element;
$this->dispatcher = $dispatcher;
return $this;
}




public function getDispatcher() {
return $this->dispatcher;
}





protected function dispatch($method) {
if (!$this->dispatcher) {
return;
}
$arguments = func_get_args();
unset($arguments[0]);
$this->dispatcher->dispatch($method, $arguments);
}




public function getElement() {
return $this->element;
}





protected function newElement(WebDriverElement $element) {
return new static($element, $this->getDispatcher());
}






public function sendKeys($value) {

$this->dispatch('beforeChangeValueOf', $this);
try {
$this->element->sendKeys($value);
} catch (WebDriverException $exception) {
$this->dispatchOnException($exception);
}
$this->dispatch('afterChangeValueOf', $this);
return $this;

}





public function click() {
$this->dispatch('beforeClickOn', $this);
try {
$this->element->click();
} catch (WebDriverException $exception) {
$this->dispatchOnException($exception);
}
$this->dispatch('afterClickOn', $this);
return $this;
}






public function findElement(WebDriverBy $by) {
$this->dispatch(
'beforeFindBy',
$by,
$this,
$this->dispatcher->getDefaultDriver());
try {
$element = $this->newElement($this->element->findElement($by));
} catch (WebDriverException $exception) {
$this->dispatchOnException($exception);
}
$this->dispatch(
'afterFindBy',
$by,
$this,
$this->dispatcher->getDefaultDriver()
);
return $element;
}






public function findElements(WebDriverBy $by) {
$this->dispatch(
'beforeFindBy',
$by,
$this,
$this->dispatcher->getDefaultDriver()
);
try {
$elements = array();
foreach ($this->element->findElements($by) as $element) {
$elements[] = $this->newElement($element);
}
} catch (WebDriverException $exception) {
$this->dispatchOnException($exception);
}
$this->dispatch(
'afterFindBy',
$by,
$this,
$this->dispatcher->getDefaultDriver()
);
return $elements;
}





public function clear() {
try {
$this->element->clear();
return $this;
} catch (WebDriverException $exception) {
$this->dispatchOnException($exception);
}
}






public function getAttribute($attribute_name) {
try {
return $this->element->getAttribute($attribute_name);
} catch (WebDriverException $exception) {
$this->dispatchOnException($exception);
}
}






public function getCSSValue($css_property_name) {
try {
return $this->element->getCSSValue($css_property_name);
} catch (WebDriverException $exception) {
$this->dispatchOnException($exception);
}
}





public function getLocation() {
try {
return $this->element->getLocation();
} catch (WebDriverException $exception) {
$this->dispatchOnException($exception);
}
}





public function getLocationOnScreenOnceScrolledIntoView() {
try {
return $this->element->getLocationOnScreenOnceScrolledIntoView();
} catch (WebDriverException $exception) {
$this->dispatchOnException($exception);
}
}




public function getCoordinates() {
try {
return $this->element->getCoordinates();
} catch (WebDriverException $exception) {
$this->dispatchOnException($exception);
}
}






public function getSize() {
try {
return $this->element->getSize();
} catch (WebDriverException $exception) {
$this->dispatchOnException($exception);
}
}





public function getTagName() {
try {
return $this->element->getTagName();
} catch (WebDriverException $exception) {
$this->dispatchOnException($exception);
}
}





public function getText() {
try {
return $this->element->getText();
} catch (WebDriverException $exception) {
$this->dispatchOnException($exception);
}
}





public function isDisplayed() {
try {
return $this->element->isDisplayed();
} catch (WebDriverException $exception) {
$this->dispatchOnException($exception);
}
}





public function isEnabled() {
try {
return $this->element->isEnabled();
} catch (WebDriverException $exception) {
$this->dispatchOnException($exception);
}
}





public function isSelected() {
try {
return $this->element->isSelected();
} catch (WebDriverException $exception) {
$this->dispatchOnException($exception);
}
}





public function submit() {
try {
$this->element->submit();
return $this;
} catch (WebDriverException $exception) {
$this->dispatchOnException($exception);
}
}





public function getID() {
try {
return $this->element->getID();
} catch (WebDriverException $exception) {
$this->dispatchOnException($exception);
}
}








public function equals(WebDriverElement $other) {
try {
return $this->element->equals($other);
} catch (WebDriverException $exception) {
$this->dispatchOnException($exception);
}
}

private function dispatchOnException($exception) {
$this->dispatch(
'onException',
$exception,
$this->dispatcher->getDefaultDriver()
);
throw $exception;
}


}
