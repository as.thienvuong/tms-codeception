<?php






















namespace Facebook;







class GraphSessionInfo extends GraphObject
{






public function getAppId()
{
return $this->getProperty('app_id');
}






public function getApplication()
{
return $this->getProperty('application');
}






public function getExpiresAt()
{
$stamp = $this->getProperty('expires_at');
if ($stamp) {
return (new \DateTime())->setTimestamp($stamp);
} else {
return null;
}
}






public function isValid()
{
return $this->getProperty('is_valid');
}






public function getIssuedAt()
{
$stamp = $this->getProperty('issued_at');
if ($stamp) {
return (new \DateTime())->setTimestamp($stamp);
} else {
return null;
}
}






public function getScopes()
{
return $this->getPropertyAsArray('scopes');
}






public function getId()
{
return $this->getProperty('user_id');
}

}