<?php






















namespace Facebook;







class GraphLocation extends GraphObject
{






public function getStreet()
{
return $this->getProperty('street');
}






public function getCity()
{
return $this->getProperty('city');
}






public function getState()
{
return $this->getProperty('state');
}






public function getCountry()
{
return $this->getProperty('country');
}






public function getZip()
{
return $this->getProperty('zip');
}






public function getLatitude()
{
return $this->getProperty('latitude');
}






public function getLongitude()
{
return $this->getProperty('longitude');
}

}