<?php














namespace Facebook\WebDriver\Support\Events;

use Facebook\WebDriver\Exception\WebDriverException;
use Facebook\WebDriver\WebDriverDispatcher;
use Facebook\WebDriver\WebDriverNavigation;

class EventFiringWebDriverNavigation {




protected $navigator;




protected $dispatcher;





public function __construct(WebDriverNavigation $navigator,
WebDriverDispatcher $dispatcher) {
$this->navigator = $navigator;
$this->dispatcher = $dispatcher;
return $this;
}




public function getDispatcher() {
return $this->dispatcher;
}





protected function dispatch($method) {
if (!$this->dispatcher) {
return;
}

$arguments = func_get_args();
unset($arguments[0]);
$this->dispatcher->dispatch($method, $arguments);
}




public function getNavigator() {
return $this->navigator;
}





public function back() {
$this->dispatch(
'beforeNavigateBack',
$this->getDispatcher()->getDefaultDriver()
);
try {
$this->navigator->back();
} catch (WebDriverException $exception) {
$this->dispatchOnException($exception);
}
$this->dispatch(
'afterNavigateBack',
$this->getDispatcher()->getDefaultDriver()
);
return $this;
}





public function forward() {
$this->dispatch(
'beforeNavigateForward',
$this->getDispatcher()->getDefaultDriver()
);
try {
$this->navigator->forward();
} catch (WebDriverException $exception) {
$this->dispatchOnException($exception);
}
$this->dispatch(
'afterNavigateForward',
$this->getDispatcher()->getDefaultDriver()
);
return $this;
}





public function refresh() {
try {
$this->navigator->refresh();
return $this;
} catch (WebDriverException $exception) {
$this->dispatchOnException($exception);
}
}






public function to($url) {
$this->dispatch(
'beforeNavigateTo',
$url,
$this->getDispatcher()->getDefaultDriver()
);
try {
$this->navigator->to($url);
} catch (WebDriverException $exception) {
$this->dispatchOnException($exception);
}
$this->dispatch(
'afterNavigateTo',
$url,
$this->getDispatcher()->getDefaultDriver()
);
return $this;
}

private function dispatchOnException($exception) {
$this->dispatch('onException', $exception);
throw $exception;
}
}
