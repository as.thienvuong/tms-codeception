<?php


namespace Facebook\WebDriver;

use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;

require_once('vendor/autoload.php');


$host = 'http://localhost:4444/wd/hub'; 
$capabilities = DesiredCapabilities::firefox();
$driver = RemoteWebDriver::create($host, $capabilities, 5000);


$driver->get('http://docs.seleniumhq.org/');


$driver->manage()->deleteAllCookies();
$driver->manage()->addCookie(array(
'name' => 'cookie_name',
'value' => 'cookie_value',
));
$cookies = $driver->manage()->getCookies();
print_r($cookies);


$link = $driver->findElement(
WebDriverBy::id('menu_about')
);
$link->click();


echo "The title is " . $driver->getTitle() . "'\n";


echo "The current URI is " . $driver->getCurrentURL() . "'\n";


$input = $driver->findElement(
WebDriverBy::id('q')
);
$input->sendKeys('php')->submit();


$driver->wait(10)->until(
WebDriverExpectedCondition::presenceOfAllElementsLocatedBy(
WebDriverBy::className('gsc-result')
)
);


$driver->quit();
