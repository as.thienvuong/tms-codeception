<?php














namespace Facebook\WebDriver\Remote;

use Facebook\WebDriver\Exception\WebDriverException;
use Facebook\WebDriver\Interactions\Internal\WebDriverCoordinates;
use Facebook\WebDriver\Internal\WebDriverLocatable;
use Facebook\WebDriver\WebDriverBy;
use Facebook\WebDriver\WebDriverDimension;
use Facebook\WebDriver\WebDriverElement;
use Facebook\WebDriver\WebDriverKeys;
use Facebook\WebDriver\WebDriverPoint;
use ZipArchive;




class RemoteWebElement implements WebDriverElement, WebDriverLocatable {




protected $executor;



protected $id;



protected $fileDetector;





public function __construct(RemoteExecuteMethod $executor, $id) {
$this->executor = $executor;
$this->id = $id;
$this->fileDetector = new UselessFileDetector();
}







public function clear() {
$this->executor->execute(
DriverCommand::CLEAR_ELEMENT,
array(':id' => $this->id)
);
return $this;
}






public function click() {
$this->executor->execute(
DriverCommand::CLICK_ELEMENT,
array(':id' => $this->id)
);
return $this;
}










public function findElement(WebDriverBy $by) {
$params = array(
'using' => $by->getMechanism(),
'value' => $by->getValue(),
':id' => $this->id,
);
$raw_element = $this->executor->execute(
DriverCommand::FIND_CHILD_ELEMENT,
$params
);

return $this->newElement($raw_element['ELEMENT']);
}









public function findElements(WebDriverBy $by) {
$params = array(
'using' => $by->getMechanism(),
'value' => $by->getValue(),
':id' => $this->id,
);
$raw_elements = $this->executor->execute(
DriverCommand::FIND_CHILD_ELEMENTS,
$params
);

$elements = array();
foreach ($raw_elements as $raw_element) {
$elements[] = $this->newElement($raw_element['ELEMENT']);
}
return $elements;
}







public function getAttribute($attribute_name) {
$params = array(
':name' => $attribute_name,
':id' => $this->id,
);
return $this->executor->execute(
DriverCommand::GET_ELEMENT_ATTRIBUTE,
$params
);
}







public function getCSSValue($css_property_name) {
$params = array(
':propertyName' => $css_property_name,
':id' => $this->id,
);
return $this->executor->execute(
DriverCommand::GET_ELEMENT_VALUE_OF_CSS_PROPERTY,
$params
);
}






public function getLocation() {
$location = $this->executor->execute(
DriverCommand::GET_ELEMENT_LOCATION,
array(':id' => $this->id)
);
return new WebDriverPoint($location['x'], $location['y']);
}







public function getLocationOnScreenOnceScrolledIntoView() {
$location = $this->executor->execute(
DriverCommand::GET_ELEMENT_LOCATION_ONCE_SCROLLED_INTO_VIEW,
array(':id' => $this->id)
);
return new WebDriverPoint($location['x'], $location['y']);
}




public function getCoordinates() {
$element = $this;

$on_screen = null; 
 $in_view_port = function () use ($element) {
return $element->getLocationOnScreenOnceScrolledIntoView();
};
$on_page = function () use ($element) {
return $element->getLocation();
};
$auxiliary = $this->getID();

return new WebDriverCoordinates(
$on_screen,
$in_view_port,
$on_page,
$auxiliary
);
}






public function getSize() {
$size = $this->executor->execute(
DriverCommand::GET_ELEMENT_SIZE,
array(':id' => $this->id)
);
return new WebDriverDimension($size['width'], $size['height']);
}






public function getTagName() {

 
 
 
 return strtolower($this->executor->execute(
DriverCommand::GET_ELEMENT_TAG_NAME,
array(':id' => $this->id)
));
}







public function getText() {
return $this->executor->execute(
DriverCommand::GET_ELEMENT_TEXT,
array(':id' => $this->id)
);
}







public function isDisplayed() {
return $this->executor->execute(
DriverCommand::IS_ELEMENT_DISPLAYED,
array(':id' => $this->id)
);
}







public function isEnabled() {
return $this->executor->execute(
DriverCommand::IS_ELEMENT_ENABLED,
array(':id' => $this->id)
);
}






public function isSelected() {
return $this->executor->execute(
DriverCommand::IS_ELEMENT_SELECTED,
array(':id' => $this->id)
);
}







public function sendKeys($value) {
$local_file = $this->fileDetector->getLocalFile($value);
if ($local_file === null) {
$params = array(
'value' => WebDriverKeys::encode($value),
':id' => $this->id,
);
$this->executor->execute(DriverCommand::SEND_KEYS_TO_ELEMENT, $params);
} else {
$remote_path = $this->upload($local_file);
$params = array(
'value' => WebDriverKeys::encode($remote_path),
':id' => $this->id,
);
$this->executor->execute(DriverCommand::SEND_KEYS_TO_ELEMENT, $params);
}
return $this;
}









private function upload($local_file) {
if (!is_file($local_file)) {
throw new WebDriverException("You may only upload files: " . $local_file);
}


 $temp_zip = tempnam('', 'WebDriverZip');
$zip = new ZipArchive();
if ($zip->open($temp_zip, ZipArchive::CREATE) !== true) {
return false;
}
$info = pathinfo($local_file);
$file_name = $info['basename'];
$zip->addFile($local_file, $file_name);
$zip->close();
$params = array(
'file' => base64_encode(file_get_contents($temp_zip)),
);
$remote_path = $this->executor->execute(
DriverCommand::UPLOAD_FILE,
$params
);
unlink($temp_zip);
return $remote_path;
}
















public function setFileDetector(FileDetector $detector) {
$this->fileDetector = $detector;
return $this;
}







public function submit() {
$this->executor->execute(
DriverCommand::SUBMIT_ELEMENT,
array(':id' => $this->id)
);

return $this;
}






public function getID() {
return $this->id;
}







public function equals(WebDriverElement $other) {
return $this->executor->execute(DriverCommand::ELEMENT_EQUALS, array(
':id' => $this->id,
':other' => $other->getID(),
));
}








protected function newElement($id) {
return new static($this->executor, $id);
}
}
