<?php














namespace Facebook\WebDriver\Chrome;

use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\DriverCommand;
use Facebook\WebDriver\Remote\Service\DriverCommandExecutor;
use Facebook\WebDriver\Exception\WebDriverException;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\Remote\WebDriverCommand;

class ChromeDriver extends RemoteWebDriver {

public static function start(
DesiredCapabilities $desired_capabilities = null,
ChromeDriverService $service = null
) {
if ($desired_capabilities === null) {
$desired_capabilities = DesiredCapabilities::chrome();
}
if ($service === null) {
$service = ChromeDriverService::createDefaultService();
}
$executor = new DriverCommandExecutor($service);
$driver = new static();
$driver->setCommandExecutor($executor)
->startSession($desired_capabilities);
return $driver;
}

public function startSession($desired_capabilities) {
$command = new WebDriverCommand(
null,
DriverCommand::NEW_SESSION,
array(
'desiredCapabilities' => $desired_capabilities->toArray(),
)
);
$response = $this->executor->execute($command);
$this->setSessionID($response->getSessionID());
}











public static function create(
$url = 'http://localhost:4444/wd/hub',
$desired_capabilities = null,
$timeout_in_ms = null,
$request_timeout_in_ms = null
) {
throw new WebDriverException('Please use ChromeDriver::start() instead.');
}









public static function createBySessionID(
$session_id,
$url = 'http://localhost:4444/wd/hub'
) {
throw new WebDriverException('Please use ChromeDriver::start() instead.');
}
}
