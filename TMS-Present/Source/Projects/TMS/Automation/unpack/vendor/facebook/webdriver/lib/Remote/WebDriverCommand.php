<?php














namespace Facebook\WebDriver\Remote;

class WebDriverCommand {

private $sessionID;
private $name;
private $parameters;

public function __construct($session_id, $name, $parameters) {
$this->sessionID = $session_id;
$this->name = $name;
$this->parameters = $parameters;
}

public function getName() {
return $this->name;
}

public function getSessionID() {
return $this->sessionID;
}

public function getParameters() {
return $this->parameters;
}
}
