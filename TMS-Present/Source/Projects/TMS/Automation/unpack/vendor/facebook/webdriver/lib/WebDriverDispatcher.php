<?php














namespace Facebook\WebDriver;

use Facebook\WebDriver\Support\Events\EventFiringWebDriver;

class WebDriverDispatcher {




protected $listeners = array();




protected $driver = null;








public function setDefaultDriver(EventFiringWebDriver $driver) {
$this->driver = $driver;
return $this;
}




public function getDefaultDriver() {
return $this->driver;
}





public function register(WebDriverEventListener $listener) {
$this->listeners[] = $listener;
return $this;
}





public function unregister(WebDriverEventListener $listener) {
$key = array_search($listener, $this->listeners, true);
if ($key !== false) {
unset($this->listeners[$key]);
}
return $this;
}






public function dispatch($method, $arguments) {
foreach ($this->listeners as $listener) {
call_user_func_array(array($listener, $method), $arguments);
}
return $this;
}

}
