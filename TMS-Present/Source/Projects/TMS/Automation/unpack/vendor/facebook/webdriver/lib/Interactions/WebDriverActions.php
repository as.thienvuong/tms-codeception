<?php














namespace Facebook\WebDriver\Interactions;

use Facebook\WebDriver\Interactions\Internal\WebDriverClickAndHoldAction;
use Facebook\WebDriver\Interactions\Internal\WebDriverMoveToOffsetAction;
use Facebook\WebDriver\Interactions\Internal\WebDriverButtonReleaseAction;
use Facebook\WebDriver\Interactions\Internal\WebDriverDoubleClickAction;
use Facebook\WebDriver\Interactions\Internal\WebDriverSendKeysAction;
use Facebook\WebDriver\Interactions\Internal\WebDriverClickAction;
use Facebook\WebDriver\Interactions\Internal\WebDriverKeyDownAction;
use Facebook\WebDriver\Interactions\Internal\WebDriverContextClickAction;
use Facebook\WebDriver\Interactions\Internal\WebDriverKeyUpAction;
use Facebook\WebDriver\Interactions\Internal\WebDriverMouseMoveAction;
use Facebook\WebDriver\WebDriverElement;
use Facebook\WebDriver\WebDriver;




class WebDriverActions {

protected $driver;
protected $keyboard;
protected $mouse;
protected $action;

public function __construct(WebDriver $driver) {
$this->driver = $driver;
$this->keyboard = $driver->getKeyboard();
$this->mouse = $driver->getMouse();
$this->action = new WebDriverCompositeAction();
}





public function perform() {
$this->action->perform();
}








public function click(WebDriverElement $element = null) {
$this->action->addAction(
new WebDriverClickAction($this->mouse, $element)
);
return $this;
}








public function clickAndHold(WebDriverElement $element = null) {
$this->action->addAction(
new WebDriverClickAndHoldAction($this->mouse, $element)
);
return $this;
}








public function contextClick(WebDriverElement $element = null) {
$this->action->addAction(
new WebDriverContextClickAction($this->mouse, $element)
);
return $this;
}








public function doubleClick(WebDriverElement $element = null) {
$this->action->addAction(
new WebDriverDoubleClickAction($this->mouse, $element)
);
return $this;
}








public function dragAndDrop(WebDriverElement $source,
WebDriverElement $target) {
$this->action->addAction(
new WebDriverClickAndHoldAction($this->mouse, $source)
);
$this->action->addAction(
new WebDriverMouseMoveAction($this->mouse, $target)
);
$this->action->addAction(
new WebDriverButtonReleaseAction($this->mouse, $target)
);
return $this;
}









public function dragAndDropBy(WebDriverElement $source,
$x_offset,
$y_offset) {
$this->action->addAction(
new WebDriverClickAndHoldAction($this->mouse, $source)
);
$this->action->addAction(
new WebDriverMoveToOffsetAction($this->mouse, null, $x_offset, $y_offset)
);
$this->action->addAction(
new WebDriverButtonReleaseAction($this->mouse, null)
);
return $this;
}








public function moveByOffset($x_offset, $y_offset) {
$this->action->addAction(
new WebDriverMoveToOffsetAction($this->mouse, null, $x_offset, $y_offset)
);
return $this;
}










public function moveToElement(WebDriverElement $element,
$x_offset = null,
$y_offset = null) {
$this->action->addAction(new WebDriverMoveToOffsetAction(
$this->mouse, $element, $x_offset, $y_offset
));
return $this;
}








public function release(WebDriverElement $element = null) {
$this->action->addAction(
new WebDriverButtonReleaseAction($this->mouse, $element)
);
return $this;
}










public function keyDown(WebDriverElement $element = null, $key = null) {
$this->action->addAction(
new WebDriverKeyDownAction($this->keyboard, $this->mouse, $element, $key)
);
return $this;
}










public function keyUp(WebDriverElement $element = null, $key = null) {
$this->action->addAction(
new WebDriverKeyUpAction($this->keyboard, $this->mouse, $element, $key)
);
return $this;
}










public function sendKeys(WebDriverElement $element = null, $keys = null) {
$this->action->addAction(
new WebDriverSendKeysAction(
$this->keyboard, $this->mouse, $element, $keys
)
);
return $this;
}
}
