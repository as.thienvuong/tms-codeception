<?php






















namespace Facebook;







class GraphObject
{




protected $backingData;






public function __construct($raw)
{
if ($raw instanceof \stdClass) {
$raw = get_object_vars($raw);
}
$this->backingData = $raw;

if (isset($this->backingData['data']) && count($this->backingData) === 1) {
if ($this->backingData['data'] instanceof \stdClass) {
$this->backingData = get_object_vars($this->backingData['data']);
} else {
$this->backingData = $this->backingData['data'];
}
}
}











public function cast($type)
{
if ($this instanceof $type) {
return $this;
}
if (is_subclass_of($type, GraphObject::className())) {
return new $type($this->backingData);
} else {
throw new FacebookSDKException(
'Cannot cast to an object that is not a GraphObject subclass', 620
);
}
}






public function asArray()
{
return $this->backingData;
}










public function getProperty($name, $type = 'Facebook\GraphObject')
{
if (isset($this->backingData[$name])) {
$value = $this->backingData[$name];
if (is_scalar($value)) {
return $value;
} else {
return (new GraphObject($value))->cast($type);
}
} else {
return null;
}
}














public function getPropertyAsArray($name, $type = 'Facebook\GraphObject')
{
$target = array();
if (isset($this->backingData[$name]['data'])) {
$target = $this->backingData[$name]['data'];
} else if (isset($this->backingData[$name])
&& !is_scalar($this->backingData[$name])) {
$target = $this->backingData[$name];
}
$out = array();
foreach ($target as $key => $value) {
if (is_scalar($value)) {
$out[$key] = $value;
} else {
$out[$key] = (new GraphObject($value))->cast($type);
}
}
return $out;
}






public function getPropertyNames()
{
return array_keys($this->backingData);
}






public static function className()
{
return get_called_class();
}

}