<?php














namespace Facebook\WebDriver\Remote;

use Facebook\WebDriver\Interactions\Touch\WebDriverTouchScreen;
use Facebook\WebDriver\WebDriverElement;




class RemoteTouchScreen implements WebDriverTouchScreen {




private $executor;




public function __construct(RemoteExecuteMethod $executor) {
$this->executor = $executor;
}






public function tap(WebDriverElement $element) {
$this->executor->execute(
DriverCommand::TOUCH_SINGLE_TAP,
array('element' => $element->getID())
);

return $this;
}






public function doubleTap(WebDriverElement $element) {
$this->executor->execute(
DriverCommand::TOUCH_DOUBLE_TAP,
array('element' => $element->getID())
);

return $this;
}







public function down($x, $y) {
$this->executor->execute(DriverCommand::TOUCH_DOWN, array(
'x' => $x,
'y' => $y,
));

return $this;
}








public function flick($xspeed, $yspeed) {
$this->executor->execute(DriverCommand::TOUCH_FLICK, array(
'xspeed' => $xspeed,
'yspeed' => $yspeed,
));

return $this;
}









public function flickFromElement(
WebDriverElement $element, $xoffset, $yoffset, $speed
) {
$this->executor->execute(DriverCommand::TOUCH_FLICK, array(
'xoffset' => $xoffset,
'yoffset' => $yoffset,
'element' => $element->getID(),
'speed' => $speed,
));

return $this;
}






public function longPress(WebDriverElement $element) {
$this->executor->execute(
DriverCommand::TOUCH_LONG_PRESS,
array('element' => $element->getID())
);

return $this;
}







public function move($x, $y) {
$this->executor->execute(DriverCommand::TOUCH_MOVE, array(
'x' => $x,
'y' => $y,
));

return $this;
}







public function scroll($xoffset, $yoffset) {
$this->executor->execute(DriverCommand::TOUCH_SCROLL, array(
'xoffset' => $xoffset,
'yoffset' => $yoffset,
));

return $this;
}








public function scrollFromElement(
WebDriverElement $element, $xoffset, $yoffset
) {
$this->executor->execute(DriverCommand::TOUCH_SCROLL, array(
'element' => $element->getID(),
'xoffset' => $xoffset,
'yoffset' => $yoffset,
));

return $this;
}








public function up($x, $y) {
$this->executor->execute(DriverCommand::TOUCH_UP, array(
'x' => $x,
'y' => $y,
));

return $this;
}

}
