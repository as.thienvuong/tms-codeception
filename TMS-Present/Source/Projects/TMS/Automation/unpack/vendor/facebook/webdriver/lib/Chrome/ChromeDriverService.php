<?php














namespace Facebook\WebDriver\Chrome;

use Facebook\WebDriver\Remote\Service\DriverService;

class ChromeDriverService extends DriverService {


 const CHROME_DRIVER_EXE_PROPERTY = "webdriver.chrome.driver";

public static function createDefaultService() {
$exe = getenv(self::CHROME_DRIVER_EXE_PROPERTY);
$port = 9515; 
 $args = array("--port=$port");
$service = new ChromeDriverService($exe, $port, $args);
return $service;
}

}
