<?php






















namespace Facebook;







class FacebookResponse
{




private $request;




private $responseData;




private $rawResponse;




private $etagHit;




private $etag;










public function __construct($request, $responseData, $rawResponse, $etagHit = false, $etag = null)
{
$this->request = $request;
$this->responseData = $responseData;
$this->rawResponse = $rawResponse;
$this->etagHit = $etagHit;
$this->etag = $etag;
}






public function getRequest()
{
return $this->request;
}






public function getResponse()
{
return $this->responseData;
}






public function getRawResponse()
{
return $this->rawResponse;
}






public function isETagHit()
{
return $this->etagHit;
}






public function getETag()
{
return $this->etag;
}









public function getGraphObject($type = 'Facebook\GraphObject') {
return (new GraphObject($this->responseData))->cast($type);
}









public function getGraphObjectList($type = 'Facebook\GraphObject') {
$out = array();
$data = $this->responseData->data;
$dataLength = count($data);
for ($i = 0; $i < $dataLength; $i++) {
$out[] = (new GraphObject($data[$i]))->cast($type);
}
return $out;
}







public function getRequestForNextPage()
{
return $this->handlePagination('next');
}







public function getRequestForPreviousPage()
{
return $this->handlePagination('previous');
}








private function handlePagination($direction) {
if (isset($this->responseData->paging->$direction)) {
$url = parse_url($this->responseData->paging->$direction);
parse_str($url['query'], $params);

if (isset($params['type']) && strpos($this->request->getPath(), $params['type']) !== false){
unset($params['type']);
}
return new FacebookRequest(
$this->request->getSession(),
$this->request->getMethod(),
$this->request->getPath(),
$params
);
} else {
return null;
}
}

}
