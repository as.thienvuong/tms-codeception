<?php














namespace Facebook\WebDriver;

use Facebook\WebDriver\Exception\UnexpectedTagNameException;
use Facebook\WebDriver\Exception\NoSuchElementException;
use Facebook\WebDriver\Exception\UnsupportedOperationException;




class WebDriverSelect {

private $element;
private $isMulti;

public function __construct(WebDriverElement $element) {
$tag_name = $element->getTagName();

if ($tag_name !== 'select') {
throw new UnexpectedTagNameException('select', $tag_name);
}
$this->element = $element;
$value = $element->getAttribute('multiple');
$this->isMulti = ($value === 'true');
}






public function isMultiple() {
return $this->isMulti;
}




public function getOptions() {
return $this->element->findElements(WebDriverBy::tagName('option'));
}




public function getAllSelectedOptions() {
$selected_options = array();
foreach ($this->getOptions() as $option) {
if ($option->isSelected()) {
$selected_options[] = $option;
}
}
return $selected_options;
}







public function getFirstSelectedOption() {
foreach ($this->getOptions() as $option) {
if ($option->isSelected()) {
return $option;
}
}

throw new NoSuchElementException('No options are selected');
}








public function deselectAll() {
if (!$this->isMultiple()) {
throw new UnsupportedOperationException(
'You may only deselect all options of a multi-select'
);
}

foreach ($this->getOptions() as $option) {
if ($option->isSelected()) {
$option->click();
}
}
}










public function selectByIndex($index) {
$matched = false;
foreach ($this->getOptions() as $option) {
if ($option->getAttribute('index') === (string)$index) {
if (!$option->isSelected()) {
$option->click();
if (!$this->isMultiple()) {
return;
}
}
$matched = true;
}
}
if (!$matched) {
throw new NoSuchElementException(
sprintf('Cannot locate option with index: %d', $index)
);
}
}













public function selectByValue($value) {
$matched = false;
$xpath = './/option[@value = '.$this->escapeQuotes($value).']';
$options = $this->element->findElements(WebDriverBy::xpath($xpath));

foreach ($options as $option) {
if (!$option->isSelected()) {
$option->click();
}
if (!$this->isMultiple()) {
return;
}
$matched = true;
}

if (!$matched) {
throw new NoSuchElementException(
sprintf('Cannot locate option with value: %s', $value)
);
}
}













public function selectByVisibleText($text) {
$matched = false;
$xpath = './/option[normalize-space(.) = '.$this->escapeQuotes($text).']';
$options = $this->element->findElements(WebDriverBy::xpath($xpath));

foreach ($options as $option) {
if (!$option->isSelected()) {
$option->click();
}
if (!$this->isMultiple()) {
return;
}
$matched = true;
}


 
 if (!$matched) {
foreach ($this->getOptions() as $option) {
if ($option->getText() === $text) {
if (!$option->isSelected()) {
$option->click();
}
if (!$this->isMultiple()) {
return;
}
$matched = true;
}
}
}

if (!$matched) {
throw new NoSuchElementException(
sprintf('Cannot locate option with text: %s', $text)
);
}
}







public function deselectByIndex($index) {
foreach ($this->getOptions() as $option) {
if ($option->getAttribute('index') === (string)$index &&
$option->isSelected()) {
$option->click();
}
}
}










public function deselectByValue($value) {
$xpath = './/option[@value = '.$this->escapeQuotes($value).']';
$options = $this->element->findElements(WebDriverBy::xpath($xpath));
foreach ($options as $option) {
if ($option->isSelected()) {
$option->click();
}
}
}










public function deselectByVisibleText($text) {
$xpath = './/option[normalize-space(.) = '.$this->escapeQuotes($text).']';
$options = $this->element->findElements(WebDriverBy::xpath($xpath));
foreach ($options as $option) {
if ($option->isSelected()) {
$option->click();
}
}
}








protected function escapeQuotes($to_escape) {
if (strpos($to_escape, '"') !== false && strpos($to_escape, "'") !== false) {
$substrings = explode('"', $to_escape);

$escaped = "concat(";
$first = true;
foreach ($substrings as $string) {
if (!$first) {
$escaped .= ", '\"',";
$first = false;
}
$escaped .= '"' . $string . '"';
}
return $escaped;
}

if (strpos($to_escape, '"') !== false) {
return sprintf("'%s'", $to_escape);
}

return sprintf('"%s"', $to_escape);
}

}
