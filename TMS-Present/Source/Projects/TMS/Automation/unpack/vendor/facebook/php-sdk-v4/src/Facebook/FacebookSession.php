<?php






















namespace Facebook;

use Facebook\Entities\AccessToken;
use Facebook\Entities\SignedRequest;







class FacebookSession
{




private static $defaultAppId;




private static $defaultAppSecret;




private $accessToken;




private $signedRequest;




protected static $useAppSecretProof = true;










public function __construct($accessToken, SignedRequest $signedRequest = null)
{
$this->accessToken = $accessToken instanceof AccessToken ? $accessToken : new AccessToken($accessToken);
$this->signedRequest = $signedRequest;
}






public function getToken()
{
return (string) $this->accessToken;
}






public function getAccessToken()
{
return $this->accessToken;
}






public function getSignedRequest()
{
return $this->signedRequest;
}






public function getSignedRequestData()
{
return $this->signedRequest ? $this->signedRequest->getPayload() : null;
}








public function getSignedRequestProperty($key)
{
return $this->signedRequest ? $this->signedRequest->get($key) : null;
}






public function getUserId()
{
return $this->signedRequest ? $this->signedRequest->getUserId() : null;
}


 








public function getSessionInfo($appId = null, $appSecret = null)
{
return $this->accessToken->getInfo($appId, $appSecret);
}


 









public function getLongLivedSession($appId = null, $appSecret = null)
{
$longLivedAccessToken = $this->accessToken->extend($appId, $appSecret);
return new static($longLivedAccessToken, $this->signedRequest);
}


 








public function getExchangeToken($appId = null, $appSecret = null)
{
return AccessToken::getCodeFromAccessToken($this->accessToken, $appId, $appSecret);
}


 











public function validate($appId = null, $appSecret = null, $machineId = null)
{
if ($this->accessToken->isValid($appId, $appSecret, $machineId)) {
return true;
}


 throw new FacebookSDKException(
'Session has expired, or is not valid for this app.', 601
);
}


 












public static function validateSessionInfo(GraphSessionInfo $tokenInfo,
$appId = null,
$machineId = null)
{
if (AccessToken::validateAccessToken($tokenInfo, $appId, $machineId)) {
return true;
}


 throw new FacebookSDKException(
'Session has expired, or is not valid for this app.', 601
);
}









public static function newSessionFromSignedRequest(SignedRequest $signedRequest)
{
if ($signedRequest->get('code')
&& !$signedRequest->get('oauth_token')) {
return self::newSessionAfterValidation($signedRequest);
}
$accessToken = $signedRequest->get('oauth_token');
$expiresAt = $signedRequest->get('expires', 0);
$accessToken = new AccessToken($accessToken, $expiresAt);
return new static($accessToken, $signedRequest);
}









protected static function newSessionAfterValidation(SignedRequest $signedRequest)
{
$code = $signedRequest->get('code');
$accessToken = AccessToken::getAccessTokenFromCode($code);
return new static($accessToken, $signedRequest);
}











public static function newAppSession($appId = null, $appSecret = null)
{
$targetAppId = static::_getTargetAppId($appId);
$targetAppSecret = static::_getTargetAppSecret($appSecret);
return new FacebookSession(
$targetAppId . '|' . $targetAppSecret
);
}








public static function setDefaultApplication($appId, $appSecret)
{
self::$defaultAppId = $appId;
self::$defaultAppSecret = $appSecret;
}











public static function _getTargetAppId($appId = null) {
$target = ($appId ?: self::$defaultAppId);
if (!$target) {
throw new FacebookSDKException(
'You must provide or set a default application id.', 700
);
}
return $target;
}











public static function _getTargetAppSecret($appSecret = null) {
$target = ($appSecret ?: self::$defaultAppSecret);
if (!$target) {
throw new FacebookSDKException(
'You must provide or set a default application secret.', 701
);
}
return $target;
}






public static function enableAppSecretProof($on = true)
{
static::$useAppSecretProof = ($on ? true : false);
}






public static function useAppSecretProof()
{
return static::$useAppSecretProof;
}

}
