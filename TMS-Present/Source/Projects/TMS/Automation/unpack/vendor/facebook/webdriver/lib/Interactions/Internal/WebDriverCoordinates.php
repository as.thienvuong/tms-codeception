<?php














namespace Facebook\WebDriver\Interactions\Internal;

use Closure;
use Facebook\WebDriver\Exception\UnsupportedOperationException;
use Facebook\WebDriver\WebDriverPoint;




class WebDriverCoordinates {

private
$onScreen,
$inViewPort,
$onPage,
$auxiliary;

public function __construct(
$on_screen,
Closure $in_view_port,
Closure $on_page,
$auxiliary) {

$this->onScreen = $on_screen;
$this->inViewPort = $in_view_port;
$this->onPage = $on_page;
$this->auxiliary = $auxiliary;
}





public function onScreen() {
throw new UnsupportedOperationException(
'onScreen is planned but not yet supported by Selenium'
);
}




public function inViewPort() {
return call_user_func($this->inViewPort);
}




public function onPage() {
return call_user_func($this->onPage);
}




public function getAuxiliary() {
return $this->auxiliary;
}
}
