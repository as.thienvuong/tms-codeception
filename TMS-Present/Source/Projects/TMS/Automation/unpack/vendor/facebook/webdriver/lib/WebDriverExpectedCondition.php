<?php














namespace Facebook\WebDriver;

use Facebook\WebDriver\Exception\StaleElementReferenceException;
use Facebook\WebDriver\Exception\NoSuchFrameException;
use Facebook\WebDriver\Exception\NoSuchElementException;
use Facebook\WebDriver\Exception\NoAlertOpenException;






class WebDriverExpectedCondition {





private $apply;




public function getApply() {
return $this->apply;
}

protected function __construct($apply) {
$this->apply = $apply;
}








public static function titleIs($title) {
return new WebDriverExpectedCondition(
function ($driver) use ($title) {
return $title === $driver->getTitle();
}
);
}








public static function titleContains($title) {
return new WebDriverExpectedCondition(
function ($driver) use ($title) {
return strpos($driver->getTitle(), $title) !== false;
}
);
}









public static function presenceOfElementLocated(WebDriverBy $by) {
return new WebDriverExpectedCondition(
function ($driver) use ($by) {
return $driver->findElement($by);
}
);
}










public static function visibilityOfElementLocated(WebDriverBy $by) {
return new WebDriverExpectedCondition(
function ($driver) use ($by) {
try {
$element = $driver->findElement($by);
return $element->isDisplayed() ? $element : null;
} catch (StaleElementReferenceException $e) {
return null;
}
}
);
}










public static function visibilityOf(WebDriverElement $element) {
return new WebDriverExpectedCondition(
function ($driver) use ($element) {
return $element->isDisplayed() ? $element : null;
}
);
}









public static function presenceOfAllElementsLocatedBy(WebDriverBy $by) {
return new WebDriverExpectedCondition(
function ($driver) use ($by) {
$elements = $driver->findElements($by);
return count($elements) > 0 ? $elements : null;
}
);
}









public static function textToBePresentInElement(
WebDriverBy $by, $text) {
return new WebDriverExpectedCondition(
function ($driver) use ($by, $text) {
try {
$element_text = $driver->findElement($by)->getText();
return strpos($element_text, $text) !== false;
} catch (StaleElementReferenceException $e) {
return null;
}
}
);
}









public static function textToBePresentInElementValue(
WebDriverBy $by, $text) {
return new WebDriverExpectedCondition(
function ($driver) use ($by, $text) {
try {
$element_text = $driver->findElement($by)->getAttribute('value');
return strpos($element_text, $text) !== false;
} catch (StaleElementReferenceException $e) {
return null;
}
}
);
}










public static function frameToBeAvailableAndSwitchToIt($frame_locator) {
return new WebDriverExpectedCondition(
function ($driver) use ($frame_locator) {
try {
return $driver->switchTo()->frame($frame_locator);
} catch (NoSuchFrameException $e) {
return false;
}
}
);
}









public static function invisibilityOfElementLocated(WebDriverBy $by) {
return new WebDriverExpectedCondition(
function ($driver) use ($by) {
try {
return !($driver->findElement($by)->isDisplayed());
} catch (NoSuchElementException $e) {
return true;
} catch (StaleElementReferenceException $e) {
return true;
}
}
);
}










public static function invisibilityOfElementWithText(
WebDriverBy $by, $text) {
return new WebDriverExpectedCondition(
function ($driver) use ($by, $text) {
try {
return !($driver->findElement($by)->getText() === $text);
} catch (NoSuchElementException $e) {
return true;
} catch (StaleElementReferenceException $e) {
return true;
}
}
);
}









public static function elementToBeClickable(WebDriverBy $by) {
$visibility_of_element_located =
WebDriverExpectedCondition::visibilityOfElementLocated($by);
return new WebDriverExpectedCondition(
function ($driver) use ($visibility_of_element_located) {
$element = call_user_func(
$visibility_of_element_located->getApply(),
$driver
);
try {
if ($element !== null && $element->isEnabled()) {
return $element;
} else {
return null;
}
} catch (StaleElementReferenceException $e) {
return null;
}
}
);
}








public static function stalenessOf(WebDriverElement $element) {
return new WebDriverExpectedCondition(
function ($driver) use ($element) {
try {
$element->isEnabled();
return false;
} catch (StaleElementReferenceException $e) {
return true;
}
}
);
}














public static function refreshed(WebDriverExpectedCondition $condition) {
return new WebDriverExpectedCondition(
function ($driver) use ($condition) {
try {
return call_user_func($condition->getApply(), $driver);
} catch (StaleElementReferenceException $e) {
return null;
}
}
);
}







public static function elementToBeSelected($element_or_by) {
return WebDriverExpectedCondition::elementSelectionStateToBe(
$element_or_by,
true
);
}








public static function elementSelectionStateToBe(
$element_or_by,
$selected
) {
if ($element_or_by instanceof WebDriverElement) {
return new WebDriverExpectedCondition(
function ($driver) use ($element_or_by, $selected) {
return $element_or_by->isSelected() === $selected;
}
);
} else if ($element_or_by instanceof WebDriverBy) {
return new WebDriverExpectedCondition(
function ($driver) use ($element_or_by, $selected) {
try {
$element = $driver->findElement($element_or_by);
return $element->isSelected() === $selected;
} catch (StaleElementReferenceException $e) {
return null;
}
}
);
}
}







public static function alertIsPresent() {
return new WebDriverExpectedCondition(
function ($driver) {
try {

 
 
 $alert = $driver->switchTo()->alert();
$alert->getText();
return $alert;
} catch (NoAlertOpenException $e) {
return null;
}
}
);
}







public static function not(WebDriverExpectedCondition $condition) {
return new WebDriverExpectedCondition(
function ($driver) use ($condition) {
$result = call_user_func($condition->getApply(), $driver);
return !$result;
}
);
}
}
