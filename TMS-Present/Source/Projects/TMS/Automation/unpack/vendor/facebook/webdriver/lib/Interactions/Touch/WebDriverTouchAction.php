<?php














namespace Facebook\WebDriver\Interactions\Touch;

use Facebook\WebDriver\Internal\WebDriverLocatable;
use Facebook\WebDriver\Interactions\Internal\WebDriverCoordinates;




abstract class WebDriverTouchAction {




protected $touchScreen;




protected $locationProvider;





public function __construct(
WebDriverTouchScreen $touch_screen,
WebDriverLocatable $location_provider = null
) {
$this->touchScreen = $touch_screen;
$this->locationProvider = $location_provider;
}




protected function getActionLocation() {
return $this->locationProvider !== null
? $this->locationProvider->getCoordinates() : null;
}

}
