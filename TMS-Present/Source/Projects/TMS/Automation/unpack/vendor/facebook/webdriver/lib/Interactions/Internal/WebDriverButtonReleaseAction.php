<?php














namespace Facebook\WebDriver\Interactions\Internal;

use Facebook\WebDriver\WebDriverAction;




class WebDriverButtonReleaseAction
extends WebDriverMouseAction
implements WebDriverAction {

public function perform() {
$this->mouse->mouseUp($this->getActionLocation());
}
}
