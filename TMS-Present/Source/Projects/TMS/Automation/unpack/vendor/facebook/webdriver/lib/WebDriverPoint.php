<?php














namespace Facebook\WebDriver;




class WebDriverPoint {

private $x, $y;

public function __construct($x, $y) {
$this->x = $x;
$this->y = $y;
}






public function getX() {
return $this->x;
}






public function getY() {
return $this->y;
}








public function move($new_x, $new_y) {
$this->x = $new_x;
$this->y = $new_y;
return $this;
}








public function moveBy($x_offset, $y_offset) {
$this->x += $x_offset;
$this->y += $y_offset;
return $this;
}







public function equals(WebDriverPoint $point) {
return $this->x === $point->getX() &&
$this->y === $point->getY();
}
}
