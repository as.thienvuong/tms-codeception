<?php






















namespace Facebook;






class FacebookPageTabHelper extends FacebookCanvasLoginHelper
{




protected $pageData;







public function __construct($appId = null, $appSecret = null)
{
parent::__construct($appId, $appSecret);

if (!$this->signedRequest) {
return;
}

$this->pageData = $this->signedRequest->get('page');
}









public function getPageData($key, $default = null)
{
if (isset($this->pageData[$key])) {
return $this->pageData[$key];
}
return $default;
}






public function isLiked()
{
return $this->getPageData('liked') === true;
}






public function isAdmin()
{
return $this->getPageData('admin') === true;
}






public function getPageId()
{
return $this->getPageData('id');
}

}
