<?php






















namespace Facebook;







class GraphUser extends GraphObject
{






public function getId()
{
return $this->getProperty('id');
}






public function getName()
{
return $this->getProperty('name');
}

public function getEmail()
{
return $this->getProperty('email');
}






public function getFirstName()
{
return $this->getProperty('first_name');
}






public function getMiddleName()
{
return $this->getProperty('middle_name');
}






public function getLastName()
{
return $this->getProperty('last_name');
}






public function getGender()
{
return $this->getProperty('gender');
}






public function getLink()
{
return $this->getProperty('link');
}






public function getBirthday()
{
$value = $this->getProperty('birthday');
if ($value) {
return new \DateTime($value);
}
return null;
}







public function getLocation()
{
return $this->getProperty('location', GraphLocation::className());
}






public function getTimezone()
{
return $this->getProperty('timezone');
}
}
