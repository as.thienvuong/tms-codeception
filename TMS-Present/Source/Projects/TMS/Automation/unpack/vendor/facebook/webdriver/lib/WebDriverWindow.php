<?php














namespace Facebook\WebDriver;

use Facebook\WebDriver\Exception\IndexOutOfBoundsException;
use Facebook\WebDriver\Remote\DriverCommand;




class WebDriverWindow {

protected $executor;

public function __construct($executor) {
$this->executor = $executor;
}







public function getPosition() {
$position = $this->executor->execute(
DriverCommand::GET_WINDOW_POSITION,
array(':windowHandle' => 'current')
);
return new WebDriverPoint(
$position['x'],
$position['y']
);
}







public function getSize() {
$size = $this->executor->execute(
DriverCommand::GET_WINDOW_SIZE,
array(':windowHandle' => 'current')
);
return new WebDriverDimension(
$size['width'],
$size['height']
);
}






public function maximize() {
$this->executor->execute(
DriverCommand::MAXIMIZE_WINDOW,
array(':windowHandle' => 'current')
);
return $this;
}








public function setSize(WebDriverDimension $size) {
$params = array(
'width' => $size->getWidth(),
'height' => $size->getHeight(),
':windowHandle' => 'current',
);
$this->executor->execute(DriverCommand::SET_WINDOW_SIZE, $params);
return $this;
}








public function setPosition(WebDriverPoint $position) {
$params = array(
'x' => $position->getX(),
'y' => $position->getY(),
':windowHandle' => 'current',
);
$this->executor->execute(DriverCommand::SET_WINDOW_POSITION, $params);
return $this;
}






public function getScreenOrientation() {
return $this->executor->execute(DriverCommand::GET_SCREEN_ORIENTATION);
}










public function setScreenOrientation($orientation) {
$orientation = strtoupper($orientation);
if (!in_array($orientation, array('PORTRAIT', 'LANDSCAPE'))) {
throw new IndexOutOfBoundsException(
"Orientation must be either PORTRAIT, or LANDSCAPE"
);
}

$this->executor->execute(
DriverCommand::SET_SCREEN_ORIENTATION,
array('orientation' => $orientation)
);

return $this;

}

}
