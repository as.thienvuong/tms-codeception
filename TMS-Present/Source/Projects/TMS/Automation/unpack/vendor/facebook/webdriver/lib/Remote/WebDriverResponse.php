<?php














namespace Facebook\WebDriver\Remote;

class WebDriverResponse {




private $status;




private $value;




private $sessionID;




public function __construct($session_id = null) {
$this->sessionID = $session_id;
}




public function getStatus() {
return $this->status;
}





public function setStatus($status) {
$this->status = $status;
return $this;
}




public function getValue() {
return $this->value;
}





public function setValue($value) {
$this->value = $value;
return $this;
}




public function getSessionID() {
return $this->sessionID;
}





public function setSessionID($session_id) {
$this->sessionID = $session_id;
return $this;
}
}
