<?php














namespace Facebook\WebDriver;

interface WebDriverCapabilities {




public function getBrowserName();





public function getCapability($name);




public function getPlatform();




public function getVersion();





public function is($capability_name);




public function isJavascriptEnabled();
}
