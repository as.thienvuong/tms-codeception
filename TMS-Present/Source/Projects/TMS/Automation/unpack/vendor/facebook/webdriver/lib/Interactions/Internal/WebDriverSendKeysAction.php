<?php














namespace Facebook\WebDriver\Interactions\Internal;

use Facebook\WebDriver\WebDriverAction;
use Facebook\WebDriver\WebDriverKeyboard;
use Facebook\WebDriver\Internal\WebDriverLocatable;
use Facebook\WebDriver\WebDriverMouse;

class WebDriverSendKeysAction
extends WebDriverKeysRelatedAction
implements WebDriverAction {

private $keys;







public function __construct(
WebDriverKeyboard $keyboard,
WebDriverMouse $mouse,
WebDriverLocatable $location_provider = null,
$keys = null) {
parent::__construct($keyboard, $mouse, $location_provider);
$this->keys = $keys;
}

public function perform() {
$this->focusOnElement();
$this->keyboard->sendKeys($this->keys);
}
}
