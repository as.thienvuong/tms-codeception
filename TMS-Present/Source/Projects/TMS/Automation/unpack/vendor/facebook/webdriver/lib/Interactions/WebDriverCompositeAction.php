<?php














namespace Facebook\WebDriver\Interactions;

use Facebook\WebDriver\WebDriverAction;




class WebDriverCompositeAction implements WebDriverAction {

private $actions = array();







public function addAction(WebDriverAction $action) {
$this->actions[] = $action;
return $this;
}






public function getNumberOfActions() {
return count($this->actions);
}




public function perform() {
foreach ($this->actions as $action) {
$action->perform();
}
}
}
