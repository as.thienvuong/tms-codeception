<?php














namespace Facebook\WebDriver\Remote;

use Facebook\WebDriver\Interactions\WebDriverActions;
use Facebook\WebDriver\JavaScriptExecutor;
use Facebook\WebDriver\WebDriver;
use Facebook\WebDriver\WebDriverBy;
use Facebook\WebDriver\WebDriverCommandExecutor;
use Facebook\WebDriver\WebDriverElement;
use Facebook\WebDriver\WebDriverNavigation;
use Facebook\WebDriver\WebDriverOptions;
use Facebook\WebDriver\WebDriverWait;

class RemoteWebDriver implements WebDriver, JavaScriptExecutor {



protected $executor;



protected $sessionID;



protected $mouse;



protected $keyboard;



protected $touch;



protected $executeMethod;

protected function __construct() {}












public static function create(
$url = 'http://localhost:4444/wd/hub',
$desired_capabilities = null,
$connection_timeout_in_ms = null,
$request_timeout_in_ms = null,
$http_proxy = null,
$http_proxy_port = null
) {
$url = preg_replace('#/+$#', '', $url);


 
 if ($desired_capabilities instanceof DesiredCapabilities) {
$desired_capabilities = $desired_capabilities->toArray();
}

$executor = new HttpCommandExecutor($url, $http_proxy, $http_proxy_port);
if ($connection_timeout_in_ms !== null) {
$executor->setConnectionTimeout($connection_timeout_in_ms);
}
if ($request_timeout_in_ms !== null) {
$executor->setRequestTimeout($request_timeout_in_ms);
}

$command = new WebDriverCommand(
null,
DriverCommand::NEW_SESSION,
array('desiredCapabilities' => $desired_capabilities)
);

$response = $executor->execute($command);

$driver = new static();
$driver->setSessionID($response->getSessionID())
->setCommandExecutor($executor);
return $driver;
}












public static function createBySessionID(
$session_id,
$url = 'http://localhost:4444/wd/hub'
) {
$driver = new static();
$driver->setSessionID($session_id)
->setCommandExecutor(new HttpCommandExecutor($url));
return $driver;
}






public function close() {
$this->execute(DriverCommand::CLOSE, array());

return $this;
}









public function findElement(WebDriverBy $by) {
$params = array('using' => $by->getMechanism(), 'value' => $by->getValue());
$raw_element = $this->execute(
DriverCommand::FIND_ELEMENT,
$params
);

return $this->newElement($raw_element['ELEMENT']);
}










public function findElements(WebDriverBy $by) {
$params = array('using' => $by->getMechanism(), 'value' => $by->getValue());
$raw_elements = $this->execute(
DriverCommand::FIND_ELEMENTS,
$params
);

$elements = array();
foreach ($raw_elements as $raw_element) {
$elements[] = $this->newElement($raw_element['ELEMENT']);
}
return $elements;
}








public function get($url) {
$params = array('url' => (string)$url);
$this->execute(DriverCommand::GET, $params);

return $this;
}






public function getCurrentURL() {
return $this->execute(DriverCommand::GET_CURRENT_URL);
}






public function getPageSource() {
return $this->execute(DriverCommand::GET_PAGE_SOURCE);
}






public function getTitle() {
return $this->execute(DriverCommand::GET_TITLE);
}







public function getWindowHandle() {
return $this->execute(
DriverCommand::GET_CURRENT_WINDOW_HANDLE,
array()
);
}






public function getWindowHandles() {
return $this->execute(DriverCommand::GET_WINDOW_HANDLES, array());
}






public function quit() {
$this->execute(DriverCommand::QUIT);
$this->executor = null;
}







private function prepareScriptArguments(array $arguments) {
$args = array();
foreach ($arguments as $key => $value) {
if ($value instanceof WebDriverElement) {
$args[$key] = array('ELEMENT'=>$value->getID());
} else {
if (is_array($value)) {
$value = $this->prepareScriptArguments($value);
}
$args[$key] = $value;
}
}
return $args;
}










public function executeScript($script, array $arguments = array()) {
$params = array(
'script' => $script,
'args' => $this->prepareScriptArguments($arguments),
);
return $this->execute(DriverCommand::EXECUTE_SCRIPT, $params);
}














public function executeAsyncScript($script, array $arguments = array()) {
$params = array(
'script' => $script,
'args' => $this->prepareScriptArguments($arguments),
);
return $this->execute(
DriverCommand::EXECUTE_ASYNC_SCRIPT,
$params
);
}







public function takeScreenshot($save_as = null) {
$screenshot = base64_decode(
$this->execute(DriverCommand::SCREENSHOT)
);
if ($save_as) {
file_put_contents($save_as, $screenshot);
}
return $screenshot;
}














public function wait(
$timeout_in_second = 30,
$interval_in_millisecond = 250) {
return new WebDriverWait(
$this, $timeout_in_second, $interval_in_millisecond
);
}







public function manage() {
return new WebDriverOptions($this->getExecuteMethod());
}








public function navigate() {
return new WebDriverNavigation($this->getExecuteMethod());
}







public function switchTo() {
return new RemoteTargetLocator($this->getExecuteMethod(), $this);
}




public function getMouse() {
if (!$this->mouse) {
$this->mouse = new RemoteMouse($this->getExecuteMethod());
}
return $this->mouse;
}




public function getKeyboard() {
if (!$this->keyboard) {
$this->keyboard = new RemoteKeyboard($this->getExecuteMethod());
}
return $this->keyboard;
}




public function getTouch() {
if (!$this->touch) {
$this->touch = new RemoteTouchScreen($this->getExecuteMethod());
}
return $this->touch;
}

protected function getExecuteMethod() {
if (!$this->executeMethod) {
$this->executeMethod = new RemoteExecuteMethod($this);
}
return $this->executeMethod;
}






public function action() {
return new WebDriverActions($this);
}







protected function newElement($id) {
return new RemoteWebElement($this->getExecuteMethod(), $id);
}







public function setCommandExecutor(WebDriverCommandExecutor $executor) {
$this->executor = $executor;
return $this;
}






public function getCommandExecutor() {
return $this->executor;
}







public function setSessionID($session_id) {
$this->sessionID = $session_id;
return $this;
}






public function getSessionID() {
return $this->sessionID;
}








public static function getAllSessions(
$url = 'http://localhost:4444/wd/hub',
$timeout_in_ms = 30000
) {
$executor = new HttpCommandExecutor($url);
$executor->setConnectionTimeout($timeout_in_ms);

$command = new WebDriverCommand(
null,
DriverCommand::GET_ALL_SESSIONS,
array()
);

return $executor->execute($command)->getValue();
}

public function execute($command_name, $params = array()) {
$command = new WebDriverCommand(
$this->sessionID,
$command_name,
$params
);

$response = $this->executor->execute($command);
return $response->getValue();
}
}
