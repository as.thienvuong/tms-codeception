<?php














namespace Facebook\WebDriver;

use Facebook\WebDriver\Remote\DriverCommand;




class WebDriverTimeouts {

protected $executor;

public function __construct($executor) {
$this->executor = $executor;
}








public function implicitlyWait($seconds) {
$this->executor->execute(
DriverCommand::IMPLICITLY_WAIT,
array('ms' => $seconds * 1000)
);
return $this;
}








public function setScriptTimeout($seconds) {
$this->executor->execute(
DriverCommand::SET_SCRIPT_TIMEOUT,
array('ms' => $seconds * 1000)
);
return $this;
}








public function pageLoadTimeout($seconds) {
$this->executor->execute(DriverCommand::SET_TIMEOUT, array(
'type' => 'page load',
'ms' => $seconds * 1000,
));
return $this;
}
}
