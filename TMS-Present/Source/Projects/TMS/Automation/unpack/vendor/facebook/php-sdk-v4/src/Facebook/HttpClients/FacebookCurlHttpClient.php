<?php






















namespace Facebook\HttpClients;

use Facebook\FacebookSDKException;





class FacebookCurlHttpClient implements FacebookHttpable
{




protected $requestHeaders = array();




protected $responseHeaders = array();




protected $responseHttpStatusCode = 0;




protected $curlErrorMessage = '';




protected $curlErrorCode = 0;




protected $rawResponse;




protected $facebookCurl;




protected static $disableIPv6;




const CURL_PROXY_QUIRK_VER = 0x071E00;




const CONNECTION_ESTABLISHED = "HTTP/1.0 200 Connection established\r\n\r\n";




public function __construct(FacebookCurl $facebookCurl = null)
{
$this->facebookCurl = $facebookCurl ?: new FacebookCurl();
self::$disableIPv6 = self::$disableIPv6 ?: false;
}




public static function disableIPv6()
{
self::$disableIPv6 = true;
}







public function addRequestHeader($key, $value)
{
$this->requestHeaders[$key] = $value;
}






public function getResponseHeaders()
{
return $this->responseHeaders;
}






public function getResponseHttpStatusCode()
{
return $this->responseHttpStatusCode;
}












public function send($url, $method = 'GET', $parameters = array())
{
$this->openConnection($url, $method, $parameters);
$this->tryToSendRequest();

if ($this->curlErrorCode) {
throw new FacebookSDKException($this->curlErrorMessage, $this->curlErrorCode);
}


 list($rawHeaders, $rawBody) = $this->extractResponseHeadersAndBody();

$this->responseHeaders = self::headersToArray($rawHeaders);

$this->closeConnection();

return $rawBody;
}








public function openConnection($url, $method = 'GET', array $parameters = array())
{
$options = array(
CURLOPT_URL => $url,
CURLOPT_CONNECTTIMEOUT => 10,
CURLOPT_TIMEOUT => 60,
CURLOPT_RETURNTRANSFER => true, 
 CURLOPT_HEADER => true, 
 CURLOPT_SSL_VERIFYHOST => 2,
CURLOPT_SSL_VERIFYPEER => true,
CURLOPT_CAINFO => __DIR__ . '/certs/DigiCertHighAssuranceEVRootCA.pem',
);

if ($method !== 'GET') {
$options[CURLOPT_POSTFIELDS] = !$this->paramsHaveFile($parameters) ? http_build_query($parameters, null, '&') : $parameters;
}
if ($method === 'DELETE' || $method === 'PUT') {
$options[CURLOPT_CUSTOMREQUEST] = $method;
}

if (count($this->requestHeaders) > 0) {
$options[CURLOPT_HTTPHEADER] = $this->compileRequestHeaders();
}

if (self::$disableIPv6) {
$options[CURLOPT_IPRESOLVE] = CURL_IPRESOLVE_V4;
}

$this->facebookCurl->init();
$this->facebookCurl->setopt_array($options);
}




public function closeConnection()
{
$this->facebookCurl->close();
}




public function tryToSendRequest()
{
$this->sendRequest();
$this->curlErrorMessage = $this->facebookCurl->error();
$this->curlErrorCode = $this->facebookCurl->errno();
$this->responseHttpStatusCode = $this->facebookCurl->getinfo(CURLINFO_HTTP_CODE);
}




public function sendRequest()
{
$this->rawResponse = $this->facebookCurl->exec();
}






public function compileRequestHeaders()
{
$return = array();

foreach ($this->requestHeaders as $key => $value) {
$return[] = $key . ': ' . $value;
}

return $return;
}






public function extractResponseHeadersAndBody()
{
$headerSize = self::getHeaderSize();

$rawHeaders = mb_substr($this->rawResponse, 0, $headerSize);
$rawBody = mb_substr($this->rawResponse, $headerSize);

return array(trim($rawHeaders), trim($rawBody));
}








public static function headersToArray($rawHeaders)
{
$headers = array();


 $rawHeaders = str_replace("\r\n", "\n", $rawHeaders);


 
 $headerCollection = explode("\n\n", trim($rawHeaders));

 $rawHeader = array_pop($headerCollection);

$headerComponents = explode("\n", $rawHeader);
foreach ($headerComponents as $line) {
if (strpos($line, ': ') === false) {
$headers['http_code'] = $line;
} else {
list ($key, $value) = explode(': ', $line);
$headers[$key] = $value;
}
}

return $headers;
}






private function getHeaderSize()
{
$headerSize = $this->facebookCurl->getinfo(CURLINFO_HEADER_SIZE);

 
 if ( $this->needsCurlProxyFix() ) {

 if (preg_match('/Content-Length: (\d+)/', $this->rawResponse, $m)) {
$headerSize = mb_strlen($this->rawResponse) - $m[1];
} elseif (stripos($this->rawResponse, self::CONNECTION_ESTABLISHED) !== false) {
$headerSize += mb_strlen(self::CONNECTION_ESTABLISHED);
}
}

return $headerSize;
}







private function needsCurlProxyFix()
{
$ver = $this->facebookCurl->version();
$version = $ver['version_number'];

return $version < self::CURL_PROXY_QUIRK_VER;
}








private function paramsHaveFile(array $params)
{
foreach ($params as $value) {
if ($value instanceof \CURLFile) {
return true;
}
}

return false;
}

}
