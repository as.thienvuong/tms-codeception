<?php






















namespace Facebook;

use Facebook\Entities\SignedRequest;





abstract class FacebookSignedRequestFromInputHelper
{




protected $signedRequest;




protected $appId;




protected $appSecret;




public $state = null;







public function __construct($appId = null, $appSecret = null)
{
$this->appId = FacebookSession::_getTargetAppId($appId);
$this->appSecret = FacebookSession::_getTargetAppSecret($appSecret);

$this->instantiateSignedRequest();
}






public function instantiateSignedRequest($rawSignedRequest = null)
{
$rawSignedRequest = $rawSignedRequest ?: $this->getRawSignedRequest();

if (!$rawSignedRequest) {
return;
}

$this->signedRequest = new SignedRequest($rawSignedRequest, $this->state, $this->appSecret);
}






public function getSession()
{
if ($this->signedRequest && $this->signedRequest->hasOAuthData()) {
return FacebookSession::newSessionFromSignedRequest($this->signedRequest);
}
return null;
}






public function getSignedRequest()
{
return $this->signedRequest;
}






public function getUserId()
{
return $this->signedRequest ? $this->signedRequest->getUserId() : null;
}






abstract public function getRawSignedRequest();






public function getRawSignedRequestFromGet()
{
if (isset($_GET['signed_request'])) {
return $_GET['signed_request'];
}

return null;
}






public function getRawSignedRequestFromPost()
{
if (isset($_POST['signed_request'])) {
return $_POST['signed_request'];
}

return null;
}






public function getRawSignedRequestFromCookie()
{
$strCookieKey = 'fbsr_' . $this->appId;
if (isset($_COOKIE[$strCookieKey])) {
return $_COOKIE[$strCookieKey];
}
return null;
}

}
