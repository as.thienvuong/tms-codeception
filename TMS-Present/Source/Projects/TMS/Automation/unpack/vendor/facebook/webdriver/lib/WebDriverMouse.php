<?php














namespace Facebook\WebDriver;

use Facebook\WebDriver\Interactions\Internal\WebDriverCoordinates;




interface WebDriverMouse {





public function click(WebDriverCoordinates $where);





public function contextClick(WebDriverCoordinates $where);





public function doubleClick(WebDriverCoordinates $where);





public function mouseDown(WebDriverCoordinates $where);







public function mouseMove(WebDriverCoordinates $where,
$x_offset = null,
$y_offset = null);





public function mouseUp(WebDriverCoordinates $where);
}
