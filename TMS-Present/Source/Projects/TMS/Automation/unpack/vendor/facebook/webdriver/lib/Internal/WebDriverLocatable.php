<?php














namespace Facebook\WebDriver\Internal;

use Facebook\WebDriver\Interactions\Internal\WebDriverCoordinates;




interface WebDriverLocatable {




public function getCoordinates();
}
