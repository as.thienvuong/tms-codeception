<?php














namespace Facebook\WebDriver;




interface WebDriver extends WebDriverSearchContext {






public function close();







public function get($url);






public function getCurrentURL();






public function getPageSource();






public function getTitle();







public function getWindowHandle();






public function getWindowHandles();






public function quit();







public function takeScreenshot($save_as = null);













public function wait(
$timeout_in_second = 30,
$interval_in_millisecond = 250);







public function manage();








public function navigate();







public function switchTo();






public function execute($name, $params);
}
