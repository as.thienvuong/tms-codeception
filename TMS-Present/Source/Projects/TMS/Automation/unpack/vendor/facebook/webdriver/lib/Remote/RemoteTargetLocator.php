<?php














namespace Facebook\WebDriver\Remote;

use Facebook\WebDriver\WebDriverTargetLocator;
use Facebook\WebDriver\WebDriverElement;
use Facebook\WebDriver\WebDriverAlert;
use Facebook\WebDriver\WebDriver;




class RemoteTargetLocator implements WebDriverTargetLocator {

protected $executor;
protected $driver;

public function __construct($executor, $driver) {
$this->executor = $executor;
$this->driver = $driver;
}







public function defaultContent() {
$params = array('id' => null);
$this->executor->execute(DriverCommand::SWITCH_TO_FRAME, $params);

return $this->driver;
}








public function frame($frame) {
if ($frame instanceof WebDriverElement) {
$id = array('ELEMENT' => $frame->getID());
} else {
$id = (string)$frame;
}

$params = array('id' => $id);
$this->executor->execute(DriverCommand::SWITCH_TO_FRAME, $params);

return $this->driver;
}








public function window($handle) {
$params = array('name' => (string)$handle);
$this->executor->execute(DriverCommand::SWITCH_TO_WINDOW, $params);

return $this->driver;
}







public function alert() {
return new WebDriverAlert($this->executor);
}







public function activeElement() {
$response = $this->driver->execute(DriverCommand::GET_ACTIVE_ELEMENT);
$method = new RemoteExecuteMethod($this->driver);
return new RemoteWebElement($method, $response['ELEMENT']);
}
}
