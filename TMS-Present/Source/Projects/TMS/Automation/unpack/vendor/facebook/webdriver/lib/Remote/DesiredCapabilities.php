<?php














namespace Facebook\WebDriver\Remote;

use Exception;
use Facebook\WebDriver\Chrome\ChromeOptions;
use Facebook\WebDriver\Firefox\FirefoxDriver;
use Facebook\WebDriver\Firefox\FirefoxProfile;
use Facebook\WebDriver\WebDriverCapabilities;
use Facebook\WebDriver\WebDriverPlatform;

class DesiredCapabilities implements WebDriverCapabilities {

private $capabilities;

public function __construct(array $capabilities = array()) {
$this->capabilities = $capabilities;
}




public function getBrowserName() {
return $this->get(WebDriverCapabilityType::BROWSER_NAME, '');
}





public function setBrowserName($browser_name) {
$this->set(WebDriverCapabilityType::BROWSER_NAME, $browser_name);
return $this;
}




public function getVersion() {
return $this->get(WebDriverCapabilityType::VERSION, '');
}





public function setVersion($version) {
$this->set(WebDriverCapabilityType::VERSION, $version);
return $this;
}





public function getCapability($name) {
return $this->get($name);
}






public function setCapability($name, $value) {
$this->set($name, $value);
return $this;
}




public function getPlatform() {
return $this->get(WebDriverCapabilityType::PLATFORM, '');
}





public function setPlatform($platform) {
$this->set(WebDriverCapabilityType::PLATFORM, $platform);
return $this;
}





public function is($capability_name) {
return (bool) $this->get($capability_name);
}




public function isJavascriptEnabled() {
return $this->get(WebDriverCapabilityType::JAVASCRIPT_ENABLED, false);
}









public function setJavascriptEnabled($enabled) {
$browser = $this->getBrowserName();
if ($browser && $browser !== WebDriverBrowserType::HTMLUNIT) {
throw new Exception(
'isJavascriptEnable() is a htmlunit-only option. '.
'See https://code.google.com/p/selenium/wiki/DesiredCapabilities#Read-write_capabilities.'
);
}

$this->set(WebDriverCapabilityType::JAVASCRIPT_ENABLED, $enabled);
return $this;
}




public function toArray() {
if (isset($this->capabilities[ChromeOptions::CAPABILITY]) &&
$this->capabilities[ChromeOptions::CAPABILITY] instanceof ChromeOptions) {
$this->capabilities[ChromeOptions::CAPABILITY] =
$this->capabilities[ChromeOptions::CAPABILITY]->toArray();
}

if (isset($this->capabilities[FirefoxDriver::PROFILE]) &&
$this->capabilities[FirefoxDriver::PROFILE] instanceof FirefoxProfile) {
$this->capabilities[FirefoxDriver::PROFILE] =
$this->capabilities[FirefoxDriver::PROFILE]->encode();
}

return $this->capabilities;
}






private function set($key, $value) {
$this->capabilities[$key] = $value;
return $this;
}






private function get($key, $default = null) {
return isset($this->capabilities[$key])
? $this->capabilities[$key]
: $default;
}




public static function android() {
return new DesiredCapabilities(array(
WebDriverCapabilityType::BROWSER_NAME => WebDriverBrowserType::ANDROID,
WebDriverCapabilityType::PLATFORM => WebDriverPlatform::ANDROID,
));
}




public static function chrome() {
return new DesiredCapabilities(array(
WebDriverCapabilityType::BROWSER_NAME => WebDriverBrowserType::CHROME,
WebDriverCapabilityType::PLATFORM => WebDriverPlatform::ANY,
));
}




public static function firefox() {
$caps = new DesiredCapabilities(array(
WebDriverCapabilityType::BROWSER_NAME => WebDriverBrowserType::FIREFOX,
WebDriverCapabilityType::PLATFORM => WebDriverPlatform::ANY,
));


 $profile = new FirefoxProfile();
$profile->setPreference('reader.parse-on-load.enabled', false);
$caps->setCapability(FirefoxDriver::PROFILE, $profile);

return $caps;
}




public static function htmlUnit() {
return new DesiredCapabilities(array(
WebDriverCapabilityType::BROWSER_NAME => WebDriverBrowserType::HTMLUNIT,
WebDriverCapabilityType::PLATFORM => WebDriverPlatform::ANY,
));
}




public static function htmlUnitWithJS() {
$caps = new DesiredCapabilities(array(
WebDriverCapabilityType::BROWSER_NAME => WebDriverBrowserType::HTMLUNIT,
WebDriverCapabilityType::PLATFORM => WebDriverPlatform::ANY,
));
return $caps->setJavascriptEnabled(true);
}




public static function internetExplorer() {
return new DesiredCapabilities(array(
WebDriverCapabilityType::BROWSER_NAME => WebDriverBrowserType::IE,
WebDriverCapabilityType::PLATFORM => WebDriverPlatform::WINDOWS,
));
}




public static function iphone() {
return new DesiredCapabilities(array(
WebDriverCapabilityType::BROWSER_NAME => WebDriverBrowserType::IPHONE,
WebDriverCapabilityType::PLATFORM => WebDriverPlatform::MAC,
));
}




public static function ipad() {
return new DesiredCapabilities(array(
WebDriverCapabilityType::BROWSER_NAME => WebDriverBrowserType::IPAD,
WebDriverCapabilityType::PLATFORM => WebDriverPlatform::MAC,
));
}




public static function opera() {
return new DesiredCapabilities(array(
WebDriverCapabilityType::BROWSER_NAME => WebDriverBrowserType::OPERA,
WebDriverCapabilityType::PLATFORM => WebDriverPlatform::ANY,
));
}




public static function safari() {
return new DesiredCapabilities(array(
WebDriverCapabilityType::BROWSER_NAME => WebDriverBrowserType::SAFARI,
WebDriverCapabilityType::PLATFORM => WebDriverPlatform::ANY,
));
}




public static function phantomjs() {
return new DesiredCapabilities(array(
WebDriverCapabilityType::BROWSER_NAME => WebDriverBrowserType::PHANTOMJS,
WebDriverCapabilityType::PLATFORM => WebDriverPlatform::ANY,
));
}
}
