<?php














namespace Facebook\WebDriver\Interactions\Internal;

use Facebook\WebDriver\WebDriverAction;




class WebDriverContextClickAction
extends WebDriverMouseAction
implements WebDriverAction {

public function perform() {
$this->mouse->contextClick($this->getActionLocation());
}
}
