<?php














namespace Facebook\WebDriver\Interactions\Internal;

use Facebook\WebDriver\WebDriverAction;




class WebDriverClickAndHoldAction
extends WebDriverMouseAction
implements WebDriverAction {

public function perform() {
$this->mouse->mouseDown($this->getActionLocation());
}
}
