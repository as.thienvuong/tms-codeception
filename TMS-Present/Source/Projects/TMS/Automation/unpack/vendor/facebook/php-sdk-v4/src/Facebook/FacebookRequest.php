<?php






















namespace Facebook;

use Facebook\HttpClients\FacebookHttpable;
use Facebook\HttpClients\FacebookCurlHttpClient;
use Facebook\HttpClients\FacebookStreamHttpClient;







class FacebookRequest
{




const VERSION = '4.0.23';




const GRAPH_API_VERSION = 'v2.3';




const BASE_GRAPH_URL = 'https://graph.facebook.com';




const BASE_VIDEO_GRAPH_URL = 'https://graph-video.facebook.com';




private $session;




private $method;




private $path;




private $params;




private $version;




private $etag;




private static $httpClientHandler;




public static $requestCount = 0;






public function getSession()
{
return $this->session;
}






public function getPath()
{
return $this->path;
}






public function getParameters()
{
return $this->params;
}






public function getMethod()
{
return $this->method;
}






public function getETag()
{
return $this->etag;
}







public static function setHttpClientHandler(FacebookHttpable $handler)
{
static::$httpClientHandler = $handler;
}







public static function getHttpClientHandler()
{
if (static::$httpClientHandler) {
return static::$httpClientHandler;
}
return function_exists('curl_init') ? new FacebookCurlHttpClient() : new FacebookStreamHttpClient();
}













public function __construct(
FacebookSession $session, $method, $path, $parameters = null, $version = null, $etag = null
)
{
$this->session = $session;
$this->method = $method;
$this->path = $path;
if ($version) {
$this->version = $version;
} else {
$this->version = static::GRAPH_API_VERSION;
}
$this->etag = $etag;

$params = ($parameters ?: array());
if ($session
&& ! isset($params['access_token'])) {
$params['access_token'] = $session->getToken();
}
if (! isset($params['appsecret_proof'])
&& FacebookSession::useAppSecretProof()) {
$params['appsecret_proof'] = $this->getAppSecretProof(
$params['access_token']
);
}
$this->params = $params;
}






protected function getRequestURL()
{
$pathElements = explode('/', $this->path);
$lastInPath = end($pathElements);
if ($lastInPath == 'videos' && $this->method === 'POST') {
$baseUrl = static::BASE_VIDEO_GRAPH_URL;
} else {
$baseUrl = static::BASE_GRAPH_URL;
}
return $baseUrl . '/' . $this->version . $this->path;
}









public function execute()
{
$url = $this->getRequestURL();
$params = $this->getParameters();

if ($this->method === 'GET') {
$url = self::appendParamsToUrl($url, $params);
$params = array();
}

$connection = self::getHttpClientHandler();
$connection->addRequestHeader('User-Agent', 'fb-php-' . self::VERSION);
$connection->addRequestHeader('Accept-Encoding', '*'); 


 if (null !== $this->etag) {
$connection->addRequestHeader('If-None-Match', $this->etag);
}


 
 $result = $connection->send($url, $this->method, $params);

static::$requestCount++;

$etagHit = 304 === $connection->getResponseHttpStatusCode();

$headers = $connection->getResponseHeaders();
$etagReceived = isset($headers['ETag']) ? $headers['ETag'] : null;

$decodedResult = json_decode($result);
if ($decodedResult === null) {
$out = array();
parse_str($result, $out);
return new FacebookResponse($this, $out, $result, $etagHit, $etagReceived);
}
if (isset($decodedResult->error)) {
throw FacebookRequestException::create(
$result,
$decodedResult->error,
$connection->getResponseHttpStatusCode()
);
}

return new FacebookResponse($this, $decodedResult, $result, $etagHit, $etagReceived);
}








public function getAppSecretProof($token)
{
return hash_hmac('sha256', $token, FacebookSession::_getTargetAppSecret());
}









public static function appendParamsToUrl($url, array $params = array())
{
if (!$params) {
return $url;
}

if (strpos($url, '?') === false) {
return $url . '?' . http_build_query($params, null, '&');
}

list($path, $query_string) = explode('?', $url, 2);
parse_str($query_string, $query_array);


 $params = array_merge($params, $query_array);

return $path . '?' . http_build_query($params, null, '&');
}

}
