<?php






















namespace Facebook\Entities;

use Facebook\FacebookSDKException;
use Facebook\FacebookSession;





class SignedRequest
{




public $rawSignedRequest;




public $payload;








public function __construct($rawSignedRequest = null, $state = null, $appSecret = null)
{
if (!$rawSignedRequest) {
return;
}

$this->rawSignedRequest = $rawSignedRequest;
$this->payload = static::parse($rawSignedRequest, $state, $appSecret);
}






public function getRawSignedRequest()
{
return $this->rawSignedRequest;
}






public function getPayload()
{
return $this->payload;
}









public function get($key, $default = null)
{
if (isset($this->payload[$key])) {
return $this->payload[$key];
}
return $default;
}






public function getUserId()
{
return $this->get('user_id');
}






public function hasOAuthData()
{
return isset($this->payload['oauth_token']) || isset($this->payload['code']);
}









public static function make(array $payload, $appSecret = null)
{
$payload['algorithm'] = 'HMAC-SHA256';
$payload['issued_at'] = time();
$encodedPayload = static::base64UrlEncode(json_encode($payload));

$hashedSig = static::hashSignature($encodedPayload, $appSecret);
$encodedSig = static::base64UrlEncode($hashedSig);

return $encodedSig.'.'.$encodedPayload;
}











public static function parse($signedRequest, $state = null, $appSecret = null)
{
list($encodedSig, $encodedPayload) = static::split($signedRequest);


 $sig = static::decodeSignature($encodedSig);
$hashedSig = static::hashSignature($encodedPayload, $appSecret);
static::validateSignature($hashedSig, $sig);


 $data = static::decodePayload($encodedPayload);
static::validateAlgorithm($data);
if ($state) {
static::validateCsrf($data, $state);
}

return $data;
}








public static function validateFormat($signedRequest)
{
if (strpos($signedRequest, '.') !== false) {
return;
}

throw new FacebookSDKException(
'Malformed signed request.', 606
);
}








public static function split($signedRequest)
{
static::validateFormat($signedRequest);

return explode('.', $signedRequest, 2);
}










public static function decodeSignature($encodedSig)
{
$sig = static::base64UrlDecode($encodedSig);

if ($sig) {
return $sig;
}

throw new FacebookSDKException(
'Signed request has malformed encoded signature data.', 607
);
}










public static function decodePayload($encodedPayload)
{
$payload = static::base64UrlDecode($encodedPayload);

if ($payload) {
$payload = json_decode($payload, true);
}

if (is_array($payload)) {
return $payload;
}

throw new FacebookSDKException(
'Signed request has malformed encoded payload data.', 607
);
}








public static function validateAlgorithm(array $data)
{
if (isset($data['algorithm']) && $data['algorithm'] === 'HMAC-SHA256') {
return;
}

throw new FacebookSDKException(
'Signed request is using the wrong algorithm.', 605
);
}











public static function hashSignature($encodedData, $appSecret = null)
{
$hashedSig = hash_hmac(
'sha256', $encodedData, FacebookSession::_getTargetAppSecret($appSecret), $raw_output = true
);

if ($hashedSig) {
return $hashedSig;
}

throw new FacebookSDKException(
'Unable to hash signature from encoded payload data.', 602
);
}









public static function validateSignature($hashedSig, $sig)
{
$intSignatureLength = mb_strlen($sig);
if (mb_strlen($hashedSig) === $intSignatureLength) {
$validate = 0;
for ($i = 0; $i < $intSignatureLength; $i++) {
$validate |= ord($hashedSig[$i]) ^ ord($sig[$i]);
}
if ($validate === 0) {
return;
}
}

throw new FacebookSDKException(
'Signed request has an invalid signature.', 602
);
}









public static function validateCsrf(array $data, $state)
{
if (isset($data['state'])) {
$savedLen = strlen($state);
$givenLen = strlen($data['state']);
if ($savedLen == $givenLen) {
$result = 0;
for ($i = 0; $i < $savedLen; $i++) {
$result |= ord($state[$i]) ^ ord($data['state'][$i]);
}
if ($result === 0) {
return;
}
}
}

throw new FacebookSDKException(
'Signed request did not pass CSRF validation.', 604
);
}











public static function base64UrlDecode($input)
{
$urlDecodedBase64 = strtr($input, '-_', '+/');
static::validateBase64($urlDecodedBase64);
return base64_decode($urlDecodedBase64);
}











public static function base64UrlEncode($input)
{
return strtr(base64_encode($input), '+/', '-_');
}








public static function validateBase64($input)
{
$pattern = '/^[a-zA-Z0-9\/\r\n+]*={0,2}$/';
if (preg_match($pattern, $input)) {
return;
}

throw new FacebookSDKException(
'Signed request contains malformed base64 encoding.', 608
);
}

}
