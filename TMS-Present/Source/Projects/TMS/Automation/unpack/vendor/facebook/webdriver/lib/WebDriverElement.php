<?php














namespace Facebook\WebDriver;




interface WebDriverElement extends WebDriverSearchContext {







public function clear();






public function click();







public function getAttribute($attribute_name);







public function getCSSValue($css_property_name);






public function getLocation();







public function getLocationOnScreenOnceScrolledIntoView();






public function getSize();






public function getTagName();







public function getText();







public function isDisplayed();







public function isEnabled();






public function isSelected();







public function sendKeys($value);







public function submit();






public function getID();
}
