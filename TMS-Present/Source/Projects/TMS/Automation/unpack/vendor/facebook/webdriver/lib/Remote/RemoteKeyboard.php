<?php














namespace Facebook\WebDriver\Remote;

use Facebook\WebDriver\WebDriverKeys;
use Facebook\WebDriver\WebDriverKeyboard;




class RemoteKeyboard implements WebDriverKeyboard {




private $executor;




public function __construct(RemoteExecuteMethod $executor) {
$this->executor = $executor;
}






public function sendKeys($keys) {
$this->executor->execute(DriverCommand::SEND_KEYS_TO_ACTIVE_ELEMENT, array(
'value' => WebDriverKeys::encode($keys),
));
return $this;
}








public function pressKey($key) {
$this->executor->execute(DriverCommand::SEND_KEYS_TO_ACTIVE_ELEMENT, array(
'value' => array((string)$key),
));
return $this;
}








public function releaseKey($key) {
$this->executor->execute(DriverCommand::SEND_KEYS_TO_ACTIVE_ELEMENT, array(
'value' => array((string)$key),
));
return $this;
}
}
