<?php
namespace GuzzleHttp\Exception;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Promise\PromiseInterface;




class RequestException extends TransferException
{

private $request;


private $response;


private $handlerContext;

public function __construct(
$message,
RequestInterface $request,
ResponseInterface $response = null,
\Exception $previous = null,
array $handlerContext = []
) {

 $code = $response && !($response instanceof PromiseInterface)
? $response->getStatusCode()
: 0;
parent::__construct($message, $code, $previous);
$this->request = $request;
$this->response = $response;
$this->handlerContext = $handlerContext;
}









public static function wrapException(RequestInterface $request, \Exception $e)
{
return $e instanceof RequestException
? $e
: new RequestException($e->getMessage(), $request, null, $e);
}











public static function create(
RequestInterface $request,
ResponseInterface $response = null,
\Exception $previous = null,
array $ctx = []
) {
if (!$response) {
return new self(
'Error completing request',
$request,
null,
$previous,
$ctx
);
}

$level = floor($response->getStatusCode() / 100);
if ($level == '4') {
$label = 'Client error response';
$className = __NAMESPACE__ . '\\ClientException';
} elseif ($level == '5') {
$label = 'Server error response';
$className = __NAMESPACE__ . '\\ServerException';
} else {
$label = 'Unsuccessful response';
$className = __CLASS__;
}

$message = $label . ' [url] ' . $request->getUri()
. ' [http method] ' . $request->getMethod()
. ' [status code] ' . $response->getStatusCode()
. ' [reason phrase] ' . $response->getReasonPhrase();

return new $className($message, $request, $response, $previous, $ctx);
}






public function getRequest()
{
return $this->request;
}






public function getResponse()
{
return $this->response;
}






public function hasResponse()
{
return $this->response !== null;
}











public function getHandlerContext()
{
return $this->handlerContext;
}
}
