<?php
namespace GuzzleHttp\Psr7;

use Psr\Http\Message\StreamInterface;












class InflateStream implements StreamInterface
{
use StreamDecoratorTrait;

public function __construct(StreamInterface $stream)
{

 $stream = new LimitStream($stream, -1, 10);
$resource = StreamWrapper::getResource($stream);
stream_filter_append($resource, 'zlib.inflate', STREAM_FILTER_READ);
$this->stream = new Stream($resource);
}
}
