<?php










namespace Symfony\Component\CssSelector;

use Symfony\Component\CssSelector\Parser\Shortcut\ClassParser;
use Symfony\Component\CssSelector\Parser\Shortcut\ElementParser;
use Symfony\Component\CssSelector\Parser\Shortcut\EmptyStringParser;
use Symfony\Component\CssSelector\Parser\Shortcut\HashParser;
use Symfony\Component\CssSelector\XPath\Extension\HtmlExtension;
use Symfony\Component\CssSelector\XPath\Translator;













































class CssSelector
{
private static $html = true;











public static function toXPath($cssExpr, $prefix = 'descendant-or-self::')
{
$translator = new Translator();

if (self::$html) {
$translator->registerExtension(new HtmlExtension($translator));
}

$translator
->registerParserShortcut(new EmptyStringParser())
->registerParserShortcut(new ElementParser())
->registerParserShortcut(new ClassParser())
->registerParserShortcut(new HashParser())
;

return $translator->cssToXPath($cssExpr, $prefix);
}




public static function enableHtmlExtension()
{
self::$html = true;
}




public static function disableHtmlExtension()
{
self::$html = false;
}
}
