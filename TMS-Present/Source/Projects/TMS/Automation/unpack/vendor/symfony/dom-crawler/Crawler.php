<?php










namespace Symfony\Component\DomCrawler;

use Symfony\Component\CssSelector\CssSelector;






class Crawler extends \SplObjectStorage
{



protected $uri;




private $defaultNamespacePrefix = 'default';




private $namespaces = array();




private $baseHref;








public function __construct($node = null, $currentUri = null, $baseHref = null)
{
$this->uri = $currentUri;
$this->baseHref = $baseHref ?: $currentUri;

$this->add($node);
}




public function clear()
{
$this->removeAll($this);
}











public function add($node)
{
if ($node instanceof \DOMNodeList) {
$this->addNodeList($node);
} elseif ($node instanceof \DOMNode) {
$this->addNode($node);
} elseif (is_array($node)) {
$this->addNodes($node);
} elseif (is_string($node)) {
$this->addContent($node);
} elseif (null !== $node) {
throw new \InvalidArgumentException(sprintf('Expecting a DOMNodeList or DOMNode instance, an array, a string, or null, but got "%s".', is_object($node) ? get_class($node) : gettype($node)));
}
}











public function addContent($content, $type = null)
{
if (empty($type)) {
$type = 0 === strpos($content, '<?xml') ? 'application/xml' : 'text/html';
}


 if (!preg_match('/(x|ht)ml/i', $type, $xmlMatches)) {
return;
}

$charset = null;
if (false !== $pos = stripos($type, 'charset=')) {
$charset = substr($type, $pos + 8);
if (false !== $pos = strpos($charset, ';')) {
$charset = substr($charset, 0, $pos);
}
}


 
 if (null === $charset &&
preg_match('/\<meta[^\>]+charset *= *["\']?([a-zA-Z\-0-9_:.]+)/i', $content, $matches)) {
$charset = $matches[1];
}

if (null === $charset) {
$charset = 'ISO-8859-1';
}

if ('x' === $xmlMatches[1]) {
$this->addXmlContent($content, $charset);
} else {
$this->addHtmlContent($content, $charset);
}
}














public function addHtmlContent($content, $charset = 'UTF-8')
{
$internalErrors = libxml_use_internal_errors(true);
$disableEntities = libxml_disable_entity_loader(true);

$dom = new \DOMDocument('1.0', $charset);
$dom->validateOnParse = true;

set_error_handler(function () {throw new \Exception();});

try {


if (function_exists('mb_convert_encoding')) {
$content = mb_convert_encoding($content, 'HTML-ENTITIES', $charset);
} elseif (function_exists('iconv')) {
$content = preg_replace_callback(
'/[\x80-\xFF]+/',
function ($m) {
$m = unpack('C*', $m[0]);
$i = 1;
$entities = '';

while (isset($m[$i])) {
if (0xF0 <= $m[$i]) {
$c = (($m[$i++] - 0xF0) << 18) + (($m[$i++] - 0x80) << 12) + (($m[$i++] - 0x80) << 6) + $m[$i++] - 0x80;
} elseif (0xE0 <= $m[$i]) {
$c = (($m[$i++] - 0xE0) << 12) + (($m[$i++] - 0x80) << 6) + $m[$i++] - 0x80;
} else {
$c = (($m[$i++] - 0xC0) << 6) + $m[$i++] - 0x80;
}

$entities .= '&#'.$c.';';
}

return $entities;
},
iconv($charset, 'UTF-8', $content)
);
}
} catch (\Exception $e) {
}

restore_error_handler();

if ('' !== trim($content)) {
@$dom->loadHTML($content);
}

libxml_use_internal_errors($internalErrors);
libxml_disable_entity_loader($disableEntities);

$this->addDocument($dom);

$base = $this->filterRelativeXPath('descendant-or-self::base')->extract(array('href'));

$baseHref = current($base);
if (count($base) && !empty($baseHref)) {
if ($this->baseHref) {
$linkNode = $dom->createElement('a');
$linkNode->setAttribute('href', $baseHref);
$link = new Link($linkNode, $this->baseHref);
$this->baseHref = $link->getUri();
} else {
$this->baseHref = $baseHref;
}
}
}














public function addXmlContent($content, $charset = 'UTF-8')
{

 if (!preg_match('/xmlns:/', $content)) {
$content = str_replace('xmlns', 'ns', $content);
}

$internalErrors = libxml_use_internal_errors(true);
$disableEntities = libxml_disable_entity_loader(true);

$dom = new \DOMDocument('1.0', $charset);
$dom->validateOnParse = true;

if ('' !== trim($content)) {
@$dom->loadXML($content, LIBXML_NONET);
}

libxml_use_internal_errors($internalErrors);
libxml_disable_entity_loader($disableEntities);

$this->addDocument($dom);
}






public function addDocument(\DOMDocument $dom)
{
if ($dom->documentElement) {
$this->addNode($dom->documentElement);
}
}






public function addNodeList(\DOMNodeList $nodes)
{
foreach ($nodes as $node) {
if ($node instanceof \DOMNode) {
$this->addNode($node);
}
}
}






public function addNodes(array $nodes)
{
foreach ($nodes as $node) {
$this->add($node);
}
}






public function addNode(\DOMNode $node)
{
if ($node instanceof \DOMDocument) {
$this->attach($node->documentElement);
} else {
$this->attach($node);
}
}


 public function unserialize($serialized)
{
throw new \BadMethodCallException('A Crawler cannot be serialized.');
}

public function serialize()
{
throw new \BadMethodCallException('A Crawler cannot be serialized.');
}








public function eq($position)
{
foreach ($this as $i => $node) {
if ($i == $position) {
return $this->createSubCrawler($node);
}
}

return $this->createSubCrawler(null);
}

















public function each(\Closure $closure)
{
$data = array();
foreach ($this as $i => $node) {
$data[] = $closure($this->createSubCrawler($node), $i);
}

return $data;
}









public function slice($offset = 0, $length = -1)
{
return $this->createSubCrawler(iterator_to_array(new \LimitIterator($this, $offset, $length)));
}










public function reduce(\Closure $closure)
{
$nodes = array();
foreach ($this as $i => $node) {
if (false !== $closure($this->createSubCrawler($node), $i)) {
$nodes[] = $node;
}
}

return $this->createSubCrawler($nodes);
}






public function first()
{
return $this->eq(0);
}






public function last()
{
return $this->eq(count($this) - 1);
}








public function siblings()
{
if (!count($this)) {
throw new \InvalidArgumentException('The current node list is empty.');
}

return $this->createSubCrawler($this->sibling($this->getNode(0)->parentNode->firstChild));
}








public function nextAll()
{
if (!count($this)) {
throw new \InvalidArgumentException('The current node list is empty.');
}

return $this->createSubCrawler($this->sibling($this->getNode(0)));
}








public function previousAll()
{
if (!count($this)) {
throw new \InvalidArgumentException('The current node list is empty.');
}

return $this->createSubCrawler($this->sibling($this->getNode(0), 'previousSibling'));
}








public function parents()
{
if (!count($this)) {
throw new \InvalidArgumentException('The current node list is empty.');
}

$node = $this->getNode(0);
$nodes = array();

while ($node = $node->parentNode) {
if (1 === $node->nodeType) {
$nodes[] = $node;
}
}

return $this->createSubCrawler($nodes);
}








public function children()
{
if (!count($this)) {
throw new \InvalidArgumentException('The current node list is empty.');
}

$node = $this->getNode(0)->firstChild;

return $this->createSubCrawler($node ? $this->sibling($node) : array());
}










public function attr($attribute)
{
if (!count($this)) {
throw new \InvalidArgumentException('The current node list is empty.');
}

$node = $this->getNode(0);

return $node->hasAttribute($attribute) ? $node->getAttribute($attribute) : null;
}








public function nodeName()
{
if (!count($this)) {
throw new \InvalidArgumentException('The current node list is empty.');
}

return $this->getNode(0)->nodeName;
}








public function text()
{
if (!count($this)) {
throw new \InvalidArgumentException('The current node list is empty.');
}

return $this->getNode(0)->nodeValue;
}








public function html()
{
if (!count($this)) {
throw new \InvalidArgumentException('The current node list is empty.');
}

$html = '';
foreach ($this->getNode(0)->childNodes as $child) {
$html .= $child->ownerDocument->saveHTML($child);
}

return $html;
}














public function extract($attributes)
{
$attributes = (array) $attributes;
$count = count($attributes);

$data = array();
foreach ($this as $node) {
$elements = array();
foreach ($attributes as $attribute) {
if ('_text' === $attribute) {
$elements[] = $node->nodeValue;
} else {
$elements[] = $node->getAttribute($attribute);
}
}

$data[] = $count > 1 ? $elements : $elements[0];
}

return $data;
}













public function filterXPath($xpath)
{
$xpath = $this->relativize($xpath);


 if ('' === $xpath) {
return $this->createSubCrawler(null);
}

return $this->filterRelativeXPath($xpath);
}












public function filter($selector)
{
if (!class_exists('Symfony\\Component\\CssSelector\\CssSelector')) {
throw new \RuntimeException('Unable to filter with a CSS selector as the Symfony CssSelector is not installed (you can use filterXPath instead).');
}


 return $this->filterRelativeXPath(CssSelector::toXPath($selector));
}








public function selectLink($value)
{
$xpath = sprintf('descendant-or-self::a[contains(concat(\' \', normalize-space(string(.)), \' \'), %s) ', static::xpathLiteral(' '.$value.' ')).
sprintf('or ./img[contains(concat(\' \', normalize-space(string(@alt)), \' \'), %s)]]', static::xpathLiteral(' '.$value.' '));

return $this->filterRelativeXPath($xpath);
}








public function selectButton($value)
{
$translate = 'translate(@type, "ABCDEFGHIJKLMNOPQRSTUVWXYZ", "abcdefghijklmnopqrstuvwxyz")';
$xpath = sprintf('descendant-or-self::input[((contains(%s, "submit") or contains(%s, "button")) and contains(concat(\' \', normalize-space(string(@value)), \' \'), %s)) ', $translate, $translate, static::xpathLiteral(' '.$value.' ')).
sprintf('or (contains(%s, "image") and contains(concat(\' \', normalize-space(string(@alt)), \' \'), %s)) or @id=%s or @name=%s] ', $translate, static::xpathLiteral(' '.$value.' '), static::xpathLiteral($value), static::xpathLiteral($value)).
sprintf('| descendant-or-self::button[contains(concat(\' \', normalize-space(string(.)), \' \'), %s) or @id=%s or @name=%s]', static::xpathLiteral(' '.$value.' '), static::xpathLiteral($value), static::xpathLiteral($value));

return $this->filterRelativeXPath($xpath);
}










public function link($method = 'get')
{
if (!count($this)) {
throw new \InvalidArgumentException('The current node list is empty.');
}

$node = $this->getNode(0);

return new Link($node, $this->baseHref, $method);
}






public function links()
{
$links = array();
foreach ($this as $node) {
$links[] = new Link($node, $this->baseHref, 'get');
}

return $links;
}











public function form(array $values = null, $method = null)
{
if (!count($this)) {
throw new \InvalidArgumentException('The current node list is empty.');
}

$form = new Form($this->getNode(0), $this->uri, $method, $this->baseHref);

if (null !== $values) {
$form->setValues($values);
}

return $form;
}






public function setDefaultNamespacePrefix($prefix)
{
$this->defaultNamespacePrefix = $prefix;
}





public function registerNamespace($prefix, $namespace)
{
$this->namespaces[$prefix] = $namespace;
}






















public static function xpathLiteral($s)
{
if (false === strpos($s, "'")) {
return sprintf("'%s'", $s);
}

if (false === strpos($s, '"')) {
return sprintf('"%s"', $s);
}

$string = $s;
$parts = array();
while (true) {
if (false !== $pos = strpos($string, "'")) {
$parts[] = sprintf("'%s'", substr($string, 0, $pos));
$parts[] = "\"'\"";
$string = substr($string, $pos + 1);
} else {
$parts[] = "'$string'";
break;
}
}

return sprintf('concat(%s)', implode($parts, ', '));
}










private function filterRelativeXPath($xpath)
{
$prefixes = $this->findNamespacePrefixes($xpath);

$crawler = $this->createSubCrawler(null);

foreach ($this as $node) {
$domxpath = $this->createDOMXPath($node->ownerDocument, $prefixes);
$crawler->add($domxpath->query($xpath, $node));
}

return $crawler;
}











private function relativize($xpath)
{
$expressions = array();

$unionPattern = '/\|(?![^\[]*\])/';

 
 $nonMatchingExpression = 'a[name() = "b"]';


 foreach (preg_split($unionPattern, $xpath) as $expression) {
$expression = trim($expression);
$parenthesis = '';


 
 if (preg_match('/^[\(\s*]+/', $expression, $matches)) {
$parenthesis = $matches[0];
$expression = substr($expression, strlen($parenthesis));
}


 if (0 === strpos($expression, '/_root/')) {
$expression = './'.substr($expression, 7);
} elseif (0 === strpos($expression, 'self::*/')) {
$expression = './'.substr($expression, 8);
}


 if (empty($expression)) {
$expression = $nonMatchingExpression;
} elseif (0 === strpos($expression, '//')) {
$expression = 'descendant-or-self::'.substr($expression, 2);
} elseif (0 === strpos($expression, './/')) {
$expression = 'descendant-or-self::'.substr($expression, 3);
} elseif (0 === strpos($expression, './')) {
$expression = 'self::'.substr($expression, 2);
} elseif (0 === strpos($expression, 'child::')) {
$expression = 'self::'.substr($expression, 7);
} elseif ('/' === $expression[0] || 0 === strpos($expression, 'self::')) {

 
 $expression = $nonMatchingExpression;
} elseif ('.' === $expression[0]) {

 $expression = $nonMatchingExpression;
} elseif (0 === strpos($expression, 'descendant::')) {
$expression = 'descendant-or-self::'.substr($expression, strlen('descendant::'));
} elseif (preg_match('/^(ancestor|ancestor-or-self|attribute|following|following-sibling|namespace|parent|preceding|preceding-sibling)::/', $expression)) {

 $expression = $nonMatchingExpression;
} elseif (0 !== strpos($expression, 'descendant-or-self::')) {
$expression = 'self::'.$expression;
}
$expressions[] = $parenthesis.$expression;
}

return implode(' | ', $expressions);
}






public function getNode($position)
{
foreach ($this as $i => $node) {
if ($i == $position) {
return $node;
}
}
}







protected function sibling($node, $siblingDir = 'nextSibling')
{
$nodes = array();

do {
if ($node !== $this->getNode(0) && $node->nodeType === 1) {
$nodes[] = $node;
}
} while ($node = $node->$siblingDir);

return $nodes;
}









private function createDOMXPath(\DOMDocument $document, array $prefixes = array())
{
$domxpath = new \DOMXPath($document);

foreach ($prefixes as $prefix) {
$namespace = $this->discoverNamespace($domxpath, $prefix);
if (null !== $namespace) {
$domxpath->registerNamespace($prefix, $namespace);
}
}

return $domxpath;
}









private function discoverNamespace(\DOMXPath $domxpath, $prefix)
{
if (isset($this->namespaces[$prefix])) {
return $this->namespaces[$prefix];
}


 $namespaces = $domxpath->query(sprintf('(//namespace::*[name()="%s"])[last()]', $this->defaultNamespacePrefix === $prefix ? '' : $prefix));

if ($node = $namespaces->item(0)) {
return $node->nodeValue;
}
}






private function findNamespacePrefixes($xpath)
{
if (preg_match_all('/(?P<prefix>[a-z_][a-z_0-9\-\.]*+):[^"\/:]/i', $xpath, $matches)) {
return array_unique($matches['prefix']);
}

return array();
}








private function createSubCrawler($nodes)
{
$crawler = new static($nodes, $this->uri, $this->baseHref);

return $crawler;
}
}
