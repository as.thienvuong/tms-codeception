<?php










namespace Symfony\Component\DomCrawler\Field;






abstract class FormField
{



protected $node;



protected $name;



protected $value;



protected $document;



protected $xpath;



protected $disabled;






public function __construct(\DOMElement $node)
{
$this->node = $node;
$this->name = $node->getAttribute('name');
$this->xpath = new \DOMXPath($node->ownerDocument);

$this->initialize();
}






public function getName()
{
return $this->name;
}






public function getValue()
{
return $this->value;
}






public function setValue($value)
{
$this->value = (string) $value;
}






public function hasValue()
{
return true;
}






public function isDisabled()
{
return $this->node->hasAttribute('disabled');
}




abstract protected function initialize();
}
