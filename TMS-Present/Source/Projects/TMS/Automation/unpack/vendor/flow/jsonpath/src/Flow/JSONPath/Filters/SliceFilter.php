<?php
namespace Flow\JSONPath\Filters;

use Flow\JSONPath\AccessHelper;

class SliceFilter extends AbstractFilter
{





public function filter($collection)
{













$result = [];

$length = count($collection);
$start = $this->token->value['start'];
$end = $this->token->value['end'];
$step = $this->token->value['step'] ?: 1;

if ($start !== null && $end !== null) {

 }

if ($start !== null && $end === null) {

 $end = $start < 0 ? -1 : $length - 1;
}

if ($start === null && $end !== null) {
$start = 0;
}

if ($start === null && $end === null) {
$start = 0;
$end = $length - 1;
}

if ($end < 0 && $start >= 0) {
$end = ($length + $end) - 1;
}

if ($start < 0 && $end === null) {
$end = -1;
}

for ($i = $start; $i <= $end; $i += $step) {
$index = $i;

if ($i < 0) {
$index = $length + $i;
}

if (AccessHelper::keyExists($collection, $index, $this->magicIsAllowed)) {
$result[] = $collection[$index];
}
}

return $result;
}

}
